import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

const styles = {
    card: {
        minWidth: 275,
    },
    bullet: {
        display: 'inline-block',
        margin: '0 2px',
        transform: 'scale(0.8)',
    },
    title: {
        fontSize: 14,
    },
    pos: {
        marginBottom: 12,
    },
};

class SimpleCard extends Component{
    constructor (props) {
        super(props);

        this.state = {
            classes : props
        };
        this.webpageStart = this.webpageStart.bind(this);
        this.graphRewrite = this.graphRewrite.bind(this);
    }

    webpageStart(){
        var win = window.open(this.props.product.link, '_blank');
        win.focus();
    }

    graphRewrite(){
        this.props.rewriteGraphWithProduct(this.props.product.asin)
    }

    render() {
        return (
            <Card className={this.state.classes.card}>
                <CardContent>
                    <Typography className={this.state.classes.title} color="textSecondary" gutterBottom>
                        {this.props.product.date}
                    </Typography>
                    <Typography variant="h5" component="h2">
                        {this.props.product.asin}
                    </Typography>
                    <Typography component="p">
                        Click share: {this.props.product.click_share}
                        <br/>
                        Conversion share: {this.props.product.conversion_share}
                    </Typography>
                </CardContent>
                <CardActions>
                    <Button size="small" onClick={this.webpageStart} >Product page</Button>
                    <Button size="small" onClick={this.graphRewrite} >Graph</Button>
                </CardActions>
            </Card>
        );
    };
}

SimpleCard.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(SimpleCard);