import { ResponsiveLine } from '@nivo/line'
import React, { Component } from "react";
class LineChart extends Component {
    constructor(props) {
        super(props);
        this.state = {

        };
        this.computeMax = this.computeMax.bind(this);
        this.computeMin = this.computeMin.bind(this);
        this.pointClick = this.pointClick.bind(this);
    }
    computeMax() {
        var max = 0;
        for(var i = 0; i<this.props.data.length; i++){
            var aux = Math.max.apply(Math, this.props.data[i].data.map(function(o) { return o.y; }))
            if(aux > max)
                max = aux;
        }
        return max;
    }

    computeMin() {
        var min = 9999999;
        for(var i = 0; i<this.props.data.length; i++){
            var aux = Math.min.apply(Math, this.props.data[i].data.map(function(o) { return o.y; }))
            if(aux < min)
                min = aux;
        }
        return min;
    }

    pointClick(point, event) {
        this.props.setPosition(point);
    }
    render() {
        return (
            <div id = "lineChart" className="lineChart">
                <ResponsiveLine
                    onClick={this.pointClick}
                    data={this.props.data}
                    margin={{ top: 70, right: 120, bottom: 150, left: 90 }}
                    xScale={{ type: 'point' }}
                    yScale={{ type: 'linear', stacked: false,
                        min: this.computeMax(), max: this.computeMin() }}
                    curve="linear"
                    axisTop={null}
                    axisRight={null}
                    axisBottom={{
                        orient: 'bottom',
                        tickSize: 5,
                        tickPadding: 5,
                        tickRotation: 40,
                        legend: 'days',
                        legendOffset: 70,
                        legendPosition: 'middle'
                    }}
                    axisLeft={{
                        orient: 'left',
                        tickSize: 5,
                        tickPadding: 5,
                        tickRotation: 0,
                        legend: 'ranking',
                        legendOffset: -40,
                        legendPosition: 'middle'
                    }}
                    colors={{ scheme: 'set1' }}
                    pointSize={10}
                    pointColor={{ theme: 'background' }}
                    pointBorderWidth={2}
                    pointBorderColor={{ from: 'serieColor' }}
                    pointLabel="y"
                    pointLabelYOffset={-12}
                    crosshairType="top"
                    enableArea={this.props.enableArea}
                    areaOpacity={0.05}
                    useMesh={true}
                    legends={[
                        {
                            anchor: 'bottom-right',
                            direction: 'column',
                            justify: false,
                            translateX: 100,
                            translateY: 0,
                            itemsSpacing: 0,
                            itemDirection: 'left-to-right',
                            itemWidth: 100,
                            itemHeight: 20,
                            itemOpacity: 0.75,
                            symbolSize: 12,
                            symbolShape: 'circle',
                            symbolBorderColor: 'rgba(0, 0, 0, .5)',
                            effects: [
                                {
                                    on: 'hover',
                                    style: {
                                        itemBackground: 'rgba(0, 0, 0, .03)',
                                        itemOpacity: 1
                                    }
                                }
                            ]
                        }
                    ]}
                    motionStiffness={160}
                />
            </div>
        );
    }
}

export default LineChart;
