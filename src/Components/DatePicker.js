import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import BIGDATA from "../Data";
const styles = theme => ({
    container: {
        display: 'block',
        flexWrap: 'wrap',
    },
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit
    },
});
class DatePickers extends Component {
    constructor(props) {
        super(props);

        this.onChangeListener = this.onChangeListener.bind(this);
        this.getFormattedDate = this.getFormattedDate.bind(this);
        let defaultDate = this.getFormattedDate(new Date());
        this.state = {
            id : props.id,
            label : props.label,
            value : defaultDate,
            classes : props
        };

    }

    getFormattedDate(date) {
        var year = date.getFullYear();
        var month = (1 + date.getMonth()).toString();
        month = month.length > 1 ? month : '0' + month;
        var day = date.getDate().toString();
        day = day.length > 1 ? day : '0' + day;
        return year + '-' + month + '-' + day;
    }

    onChangeListener(e) {
        this.props.writeDate(e.target.value);
        this.setState({value : e.target.value});
    }

    render() {
        return (
            <form className={this.state.classes.container} noValidate>
                <TextField
                    id={this.state.id}
                    label={this.state.label}
                    type="date"
                    onChange={this.onChangeListener}
                    value={this.state.value}
                    className={this.state.classes.textField}
                    InputLabelProps={{
                        shrink: true
                    }}
                />
            </form>
        );
    }
}

DatePickers.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(DatePickers);