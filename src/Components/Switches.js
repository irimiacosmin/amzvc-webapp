import React from 'react';
import Switch from '@material-ui/core/Switch';

class Switches extends React.Component {
    state = {
        checked: false,
    };

    handleChange = name => event => {
        this.setState({ [name]: event.target.checked });
        this.props.writeAreaToggleValue(this.state.checked);
    };

    render() {
        return (
            <div>
                <Switch
                    checked={this.state.checked}
                    onChange={this.handleChange('checked')}
                    color="primary"
                />
            </div>
        );
    }
}

export default Switches;