function getRandomArbitrary(min, max) {
    return Math.floor(Math.random() * (max - min) + min);
}

function getRandomRank() {
    return getRandomArbitrary(1, 20);
}

function getRandomProcent() {
    return getRandomArbitrary(1,100) / 1000;
}

const TERMS = [
    'bluetooth headphones',
    'wireless earbuds',
    'iphone charger',
    'airpods',
    'headphones',
    'wireless headphones',
    'bluetooth earbuds',
    'iphone xr cases',
    'game of thrones',
    'earbuds',
    'laptop',
    'iphone 8 plus case',
    'apple watch',
    'lightning cable',
    'iphone xs max case',
    'balloons',
    'iphone 7 case',
    'iphone x case',
    'fitbit',
    'pop socket',
    'apple watch band 38mm',
    'water bottle',
    'sandals for women',
    'swimsuits for women',
    'iphone 7 plus case',
    'smart watch',
    'avengers',
    'bluetooth speakers',
    'nintendo switch',
    'teacher appreciation gifts',
    'womens tops',
    'iphone',
    'iphone charger cable',
    'youtube',
    'ipad',
    'air fryer',
    'iphone 8 case',
    'apple watch band 42mm',
    'maxi dresses for women',
    'airpod case',
    'summer dresses for women',
    'shower curtain',
    'kentucky derby hats for women',
    'marvel',
    'mothers day gifts',
    'dress for women',
    'mothers day',
    'iphone 6s case',
    'hydro flask',
    'game of thrones merchandise',
    'portable charger',
    'wireless charger',
    'desk',
    'tv',
    'alexa',
    'ps4',
    'bathing suits for women',
    'nike shoes men',
    'jumpsuits for women',
    'womens dresses',
    'cbd oil',
    'backpack',
    'tablet',
    'metal straws',
    'mothers day gifts for wife',
    'lingerie for women',
    'iphone xs case',
    'earphones',
    'derby hats for women',
    'adidas shoes men',
    'spongebob squarepants',
    'funko pop',
    'sunglasses for women',
    'toilet paper',
    'gaming chair',
    'dildo',
    'patio furniture',
    'waist trainer',
    'reusable straws',
    'shoe rack',
    'fire stick',
    'luggage',
    'iphone xr case',
    'fanny pack',
    'free kindle books',
    'airpods case',
    'lego',
    'graduation party supplies 2019',
    'iphone cable',
    'sunglasses for men',
    'dresses',
    'maternity dress',
    'weighted blanket',
    'watch',
    'baby',
    'shoes',
    'lol surprise dolls',
    'nintendo switch games'
];

let COLORS = [
  'hsl(0, 100%, 50%)',
    'hsl(100, 100%, 50%)',
    'hsl(39, 100%, 50%)',
    'hsl(63, 100%, 50%)',
    'hsl(176, 100%, 50%)',
    'hsl(197, 100%, 50%)',
    'hsl(236, 100%, 50%)',
    'hsl(298, 100%, 50%)',
    'hsl(341, 100%, 50%)',
    'hsl(219, 0%, 50%)',
    'hsl(219, 0%, 0%)',
    'hsl(50, 31%, 69%)'
];

let ASINS = [
    'B07QKMZC9F',
    'B07QX8CM5X',
    'B07PXGQC1Q',
    'B07Q2B1G56',
    'B07R9L3WY1',
    'B07PG2JL5Z',
    'B07PRVSBKD',
    'B07MXBLR5W',
    'B07KGL9531',
    'B07QL3RWZW',
    'B079TGL2BZ',
    'B07K5RT25Z',
    'B00VJ04TH0',
    'B07K39FRSL',
    'B07QG2XZC1',
    'B07K4413FX',
    'B008NCSZQ8',
    'B07R3K1H9R',
    'B07PBV3JK4',
    'B00I0GS2SS',
    'B07PVJCWXD',
    'B00UY1YTGG',
    'B07R8JY3TJ',
    'B07R3K1H9R',
    'B07R6CJYV2',
    'B07FMKHX94',
    'B0748CCF3S',
    'B06XFVKG6Y',
    'B07R6MXL4D',
    'B07M9KZ36T',
    'B07P68DTRW',
    'B07PN8PZZ3',
    'B01MUAGZ49',
    'B07K9ZWNTV',
    'B07DW2XPL2',
    'B07DMHVC6T',
    'B0777RJG6D',
    'B01N6BGHNS',
    'B074SNMLM3',
    'B014QQ4P0Y',
    'B000HX6HKC',
    'B07BTS2KWK',
    'B07K97BQDF',
    'B07NTZ3HB9',
    'B07PVJZT6L',
    'B07R6MXL4D',
    'B01EVZCT9Y',
    'B071YV88D9',
    '1943200084',
    'B01N306X3V',
    'B071CV8CG2',
    'B07LF54XRH',
    'B07GB61TQR',
    'B07H947MZW',
    'B07PYX7GQR',
    'B07MZBR1BL',
    'B075P7J8BB',
    'B06XZV3F8F',
    'B07PVJCWXD',
    'B00NHQFA1I',
    'B0792KTHKJ',
    'B0194WDVHI',
    'B06XP12K3Y',
    'B001FD5KJM',
    'B07N2FWX33',
    'B01LT820BI',
    'B073B6DY4Q',
    'B07BG8FCX8',
    'B07DCSYTSR',
    'B01DDLEZ3U',
    'B079RDW17S',
    'B01MRZ02TL',
    'B07H7CGBG7',
    'B015NRWTK8',
    'B06XZTZ7GB',
    'B00OGRMULA',
    'B07MR2G87J',
    'B004YJCP7O',
    'B01GEW27DA',
    'B07NFNBHFG',
    'B07Q44JXQC',
    'B07NYWJKD9',
    'B07QM7W5ZD',
    'B010GLZ2NC',
    'B003LQ3YXU',
    'B07K968Q1C',
    'B079QHML21',
    'B0089BPEZS',
    'B07FMKHX94',
    'B009C47CM4',
    'B005PW3OS8',
    'B00992CF6W',
    'B00C40OG22',
    'B01LWVX2RG',
    'B01N5OKGLH'
];

let BIGDATA = {
    "wireless earbuds" : {
        'id': 'wireless earbuds',
        'color': 'hsl(331, 70%, 50%)',
        'data': [
            {'x': '2019-04-01', 'y': getRandomRank(1,15),
                'products': [{'asin': 'B07QKMZC9F', 'click_share': 0.086534, 'conversion_share': 0.0}, {'asin': 'B07QFXCL2Y', 'click_share': 0.049444999999999996, 'conversion_share': 0.0}, {'asin': 'B07PJWF7KD', 'click_share': 0.034527999999999996, 'conversion_share': 0.008319}]},
            {'x': '2019-04-02', 'y': getRandomArbitrary(1,15),
                'products': [{'asin': 'B07QKMZC9F', 'click_share': 0.086534, 'conversion_share': 0.0}, {'asin': 'B07QFXCL2Y', 'click_share': 0.049444999999999996, 'conversion_share': 0.0}, {'asin': 'B07PJWF7KD', 'click_share': 0.034527999999999996, 'conversion_share': 0.008319}]},
            {'x': '2019-04-03', 'y': getRandomArbitrary(1,15),
                'products': [{'asin': 'B07QKMZC9F', 'click_share': 0.086534, 'conversion_share': 0.0}, {'asin': 'B07QFXCL2Y', 'click_share': 0.049444999999999996, 'conversion_share': 0.0}, {'asin': 'B07PJWF7KD', 'click_share': 0.034527999999999996, 'conversion_share': 0.008319}]},
            {'x': '2019-04-04', 'y': getRandomArbitrary(1,15),
                'products': [{'asin': 'B07QKMZC9F', 'click_share': 0.086534, 'conversion_share': 0.0}, {'asin': 'B07QFXCL2Y', 'click_share': 0.049444999999999996, 'conversion_share': 0.0}, {'asin': 'B07PJWF7KD', 'click_share': 0.034527999999999996, 'conversion_share': 0.008319}]},
            {'x': '2019-04-05', 'y': getRandomArbitrary(1,15),
                'products': [{'asin': 'B07QKMZC9F', 'click_share': 0.086534, 'conversion_share': 0.0}, {'asin': 'B07QFXCL2Y', 'click_share': 0.049444999999999996, 'conversion_share': 0.0}, {'asin': 'B07PJWF7KD', 'click_share': 0.034527999999999996, 'conversion_share': 0.008319}]},
            {'x': '2019-04-06', 'y': getRandomArbitrary(1,15),
                'products': [{'asin': 'B07QKMZC9F', 'click_share': 0.086534, 'conversion_share': 0.0}, {'asin': 'B07QFXCL2Y', 'click_share': 0.049444999999999996, 'conversion_share': 0.0}, {'asin': 'B07PJWF7KD', 'click_share': 0.034527999999999996, 'conversion_share': 0.008319}]},
            {'x': '2019-04-07', 'y': getRandomArbitrary(1,15),
                'products': [{'asin': 'B07QKMZC9F', 'click_share': 0.086534, 'conversion_share': 0.0}, {'asin': 'B07QFXCL2Y', 'click_share': 0.049444999999999996, 'conversion_share': 0.0}, {'asin': 'B07PJWF7KD', 'click_share': 0.034527999999999996, 'conversion_share': 0.008319}]},
            {'x': '2019-04-08', 'y': getRandomArbitrary(1,15),
                'products': [{'asin': 'B07QKMZC9F', 'click_share': 0.086534, 'conversion_share': 0.0}, {'asin': 'B07QFXCL2Y', 'click_share': 0.049444999999999996, 'conversion_share': 0.0}, {'asin': 'B07PJWF7KD', 'click_share': 0.034527999999999996, 'conversion_share': 0.008319}]},
            {'x': '2019-04-09', 'y': getRandomArbitrary(1,15),
                'products': [{'asin': 'B07QKMZC9F', 'click_share': 0.086534, 'conversion_share': 0.0}, {'asin': 'B07QFXCL2Y', 'click_share': 0.049444999999999996, 'conversion_share': 0.0}, {'asin': 'B07PJWF7KD', 'click_share': 0.034527999999999996, 'conversion_share': 0.008319}]},
            {'x': '2019-04-10', 'y': getRandomArbitrary(1,15),
                'products': [{'asin': 'B07QKMZC9F', 'click_share': 0.086534, 'conversion_share': 0.0}, {'asin': 'B07QFXCL2Y', 'click_share': 0.049444999999999996, 'conversion_share': 0.0}, {'asin': 'B07PJWF7KD', 'click_share': 0.034527999999999996, 'conversion_share': 0.008319}]},
            {'x': '2019-04-11', 'y': getRandomArbitrary(1,15),
                'products': [{'asin': 'B07QKMZC9F', 'click_share': 0.086534, 'conversion_share': 0.0}, {'asin': 'B07QFXCL2Y', 'click_share': 0.049444999999999996, 'conversion_share': 0.0}, {'asin': 'B07PJWF7KD', 'click_share': 0.034527999999999996, 'conversion_share': 0.008319}]},
            {'x': '2019-04-12', 'y': getRandomArbitrary(1,15),
                'products': [{'asin': 'B07QKMZC9F', 'click_share': 0.086534, 'conversion_share': 0.0}, {'asin': 'B07QFXCL2Y', 'click_share': 0.049444999999999996, 'conversion_share': 0.0}, {'asin': 'B07PJWF7KD', 'click_share': 0.034527999999999996, 'conversion_share': 0.008319}]},
            {'x': '2019-04-13', 'y': getRandomArbitrary(1,15),
                'products': [{'asin': 'B07QKMZC9F', 'click_share': 0.086534, 'conversion_share': 0.0}, {'asin': 'B07QFXCL2Y', 'click_share': 0.049444999999999996, 'conversion_share': 0.0}, {'asin': 'B07PJWF7KD', 'click_share': 0.034527999999999996, 'conversion_share': 0.008319}]},
            {'x': '2019-04-14', 'y': getRandomArbitrary(1,15),
                'products': [{'asin': 'B07QKMZC9F', 'click_share': 0.086534, 'conversion_share': 0.0}, {'asin': 'B07QFXCL2Y', 'click_share': 0.049444999999999996, 'conversion_share': 0.0}, {'asin': 'B07PJWF7KD', 'click_share': 0.034527999999999996, 'conversion_share': 0.008319}]},
            {'x': '2019-04-15', 'y': getRandomArbitrary(1,15),
                'products': [{'asin': 'B07QKMZC9F', 'click_share': 0.086534, 'conversion_share': 0.0}, {'asin': 'B07QFXCL2Y', 'click_share': 0.049444999999999996, 'conversion_share': 0.0}, {'asin': 'B07PJWF7KD', 'click_share': 0.034527999999999996, 'conversion_share': 0.008319}]},
            {'x': '2019-04-16', 'y': getRandomArbitrary(1,15),
                'products': [{'asin': 'B07QKMZC9F', 'click_share': 0.086534, 'conversion_share': 0.0}, {'asin': 'B07QFXCL2Y', 'click_share': 0.049444999999999996, 'conversion_share': 0.0}, {'asin': 'B07PJWF7KD', 'click_share': 0.034527999999999996, 'conversion_share': 0.008319}]},
            {'x': '2019-04-17', 'y': getRandomArbitrary(1,15),
                'products': [{'asin': 'B07QKMZC9F', 'click_share': 0.086534, 'conversion_share': 0.0}, {'asin': 'B07QFXCL2Y', 'click_share': 0.049444999999999996, 'conversion_share': 0.0}, {'asin': 'B07PJWF7KD', 'click_share': 0.034527999999999996, 'conversion_share': 0.008319}]},
            {'x': '2019-04-18', 'y': getRandomArbitrary(1,15),
                'products': [{'asin': 'B07QKMZC9F', 'click_share': 0.086534, 'conversion_share': 0.0}, {'asin': 'B07QFXCL2Y', 'click_share': 0.049444999999999996, 'conversion_share': 0.0}, {'asin': 'B07PJWF7KD', 'click_share': 0.034527999999999996, 'conversion_share': 0.008319}]},
            {'x': '2019-04-19', 'y': getRandomArbitrary(1,15),
                'products': [{'asin': 'B07QKMZC9F', 'click_share': 0.086534, 'conversion_share': 0.0}, {'asin': 'B07QFXCL2Y', 'click_share': 0.049444999999999996, 'conversion_share': 0.0}, {'asin': 'B07PJWF7KD', 'click_share': 0.034527999999999996, 'conversion_share': 0.008319}]},
            {'x': '2019-04-20', 'y': getRandomArbitrary(1,15),
                'products': [{'asin': 'B07QKMZC9F', 'click_share': 0.086534, 'conversion_share': 0.0}, {'asin': 'B07QFXCL2Y', 'click_share': 0.049444999999999996, 'conversion_share': 0.0}, {'asin': 'B07PJWF7KD', 'click_share': 0.034527999999999996, 'conversion_share': 0.008319}]},
            {'x': '2019-04-21', 'y': getRandomArbitrary(1,15),
                'products': [{'asin': 'B07QKMZC9F', 'click_share': 0.086534, 'conversion_share': 0.0}, {'asin': 'B07QFXCL2Y', 'click_share': 0.049444999999999996, 'conversion_share': 0.0}, {'asin': 'B07PJWF7KD', 'click_share': 0.034527999999999996, 'conversion_share': 0.008319}]},
            {'x': '2019-04-22', 'y': getRandomArbitrary(1,15),
                'products': [{'asin': 'B07QKMZC9F', 'click_share': 0.086534, 'conversion_share': 0.0}, {'asin': 'B07QFXCL2Y', 'click_share': 0.049444999999999996, 'conversion_share': 0.0}, {'asin': 'B07PJWF7KD', 'click_share': 0.034527999999999996, 'conversion_share': 0.008319}]},
            {'x': '2019-04-23', 'y': getRandomArbitrary(1,15),
                'products': [{'asin': 'B07QKMZC9F', 'click_share': 0.086534, 'conversion_share': 0.0}, {'asin': 'B07QFXCL2Y', 'click_share': 0.049444999999999996, 'conversion_share': 0.0}, {'asin': 'B07PJWF7KD', 'click_share': 0.034527999999999996, 'conversion_share': 0.008319}]},
            {'x': '2019-04-24', 'y': getRandomArbitrary(1,15),
                'products': [{'asin': 'B07QKMZC9F', 'click_share': 0.086534, 'conversion_share': 0.0}, {'asin': 'B07QFXCL2Y', 'click_share': 0.049444999999999996, 'conversion_share': 0.0}, {'asin': 'B07PJWF7KD', 'click_share': 0.034527999999999996, 'conversion_share': 0.008319}]},
            {'x': '2019-04-25', 'y': getRandomArbitrary(1,15),
                'products': [{'asin': 'B07QKMZC9F', 'click_share': 0.086534, 'conversion_share': 0.0}, {'asin': 'B07QFXCL2Y', 'click_share': 0.049444999999999996, 'conversion_share': 0.0}, {'asin': 'B07PJWF7KD', 'click_share': 0.034527999999999996, 'conversion_share': 0.008319}]},
            {'x': '2019-04-26', 'y': getRandomArbitrary(1,15),
                'products': [{'asin': 'B07QKMZC9F', 'click_share': 0.086534, 'conversion_share': 0.0}, {'asin': 'B07QFXCL2Y', 'click_share': 0.049444999999999996, 'conversion_share': 0.0}, {'asin': 'B07PJWF7KD', 'click_share': 0.034527999999999996, 'conversion_share': 0.008319}]},
            {'x': '2019-04-27', 'y': getRandomArbitrary(1,15),
                'products': [{'asin': 'B07QKMZC9F', 'click_share': 0.086534, 'conversion_share': 0.0}, {'asin': 'B07QFXCL2Y', 'click_share': 0.049444999999999996, 'conversion_share': 0.0}, {'asin': 'B07PJWF7KD', 'click_share': 0.034527999999999996, 'conversion_share': 0.008319}]},
            {'x': '2019-04-28', 'y': getRandomArbitrary(1,15),
                'products': [{'asin': 'B07QKMZC9F', 'click_share': 0.086534, 'conversion_share': 0.0}, {'asin': 'B07QFXCL2Y', 'click_share': 0.049444999999999996, 'conversion_share': 0.0}, {'asin': 'B07PJWF7KD', 'click_share': 0.034527999999999996, 'conversion_share': 0.008319}]},
            {'x': '2019-04-29', 'y': getRandomArbitrary(1,15),
                'products': [{'asin': 'B07QKMZC9F', 'click_share': 0.086534, 'conversion_share': 0.0}, {'asin': 'B07QFXCL2Y', 'click_share': 0.049444999999999996, 'conversion_share': 0.0}, {'asin': 'B07PJWF7KD', 'click_share': 0.034527999999999996, 'conversion_share': 0.008319}]},
            {'x': '2019-04-30', 'y': getRandomArbitrary(1,15),
                'products': [{'asin': 'B07QKMZC9F', 'click_share': 0.086534, 'conversion_share': 0.0}, {'asin': 'B07QFXCL2Y', 'click_share': 0.049444999999999996, 'conversion_share': 0.0}, {'asin': 'B07PJWF7KD', 'click_share': 0.034527999999999996, 'conversion_share': 0.008319}]}
        ]}


        //'data': [{'x': '2019-04-01', 'y': 81, 'products': [{'asin': 'B07QKMZC9F', 'click_share': 0.086534, 'conversion_share': 0.0}, {'asin': 'B07QFXCL2Y', 'click_share': 0.049444999999999996, 'conversion_share': 0.0}, {'asin': 'B07PJWF7KD', 'click_share': 0.034527999999999996, 'conversion_share': 0.008319}]}, {'x': '2019-04-02', 'y': 49, 'products': [{'asin': 'B07QKMZC9F', 'click_share': 0.086534, 'conversion_share': 0.0}, {'asin': 'B07QFXCL2Y', 'click_share': 0.049444999999999996, 'conversion_share': 0.0}, {'asin': 'B07PJWF7KD', 'click_share': 0.034527999999999996, 'conversion_share': 0.008319}]}, {'x': '2019-04-03', 'y': 22, 'products': [{'asin': 'B07QKMZC9F', 'click_share': 0.086534, 'conversion_share': 0.0}, {'asin': 'B07QFXCL2Y', 'click_share': 0.049444999999999996, 'conversion_share': 0.0}, {'asin': 'B07PJWF7KD', 'click_share': 0.034527999999999996, 'conversion_share': 0.008319}]}, {'x': '2019-04-04', 'y': 57, 'products': [{'asin': 'B07QKMZC9F', 'click_share': 0.086534, 'conversion_share': 0.0}, {'asin': 'B07QFXCL2Y', 'click_share': 0.049444999999999996, 'conversion_share': 0.0}, {'asin': 'B07PJWF7KD', 'click_share': 0.034527999999999996, 'conversion_share': 0.008319}]}, {'x': '2019-04-05', 'y': 89, 'products': [{'asin': 'B07QKMZC9F', 'click_share': 0.086534, 'conversion_share': 0.0}, {'asin': 'B07QFXCL2Y', 'click_share': 0.049444999999999996, 'conversion_share': 0.0}, {'asin': 'B07PJWF7KD', 'click_share': 0.034527999999999996, 'conversion_share': 0.008319}]}, {'x': '2019-04-06', 'y': 34, 'products': [{'asin': 'B07QKMZC9F', 'click_share': 0.086534, 'conversion_share': 0.0}, {'asin': 'B07QFXCL2Y', 'click_share': 0.049444999999999996, 'conversion_share': 0.0}, {'asin': 'B07PJWF7KD', 'click_share': 0.034527999999999996, 'conversion_share': 0.008319}]}, {'x': '2019-04-07', 'y': 23, 'products': [{'asin': 'B07QKMZC9F', 'click_share': 0.086534, 'conversion_share': 0.0}, {'asin': 'B07QFXCL2Y', 'click_share': 0.049444999999999996, 'conversion_share': 0.0}, {'asin': 'B07PJWF7KD', 'click_share': 0.034527999999999996, 'conversion_share': 0.008319}]}, {'x': '2019-04-08', 'y': 95, 'products': [{'asin': 'B07QKMZC9F', 'click_share': 0.086534, 'conversion_share': 0.0}, {'asin': 'B07QFXCL2Y', 'click_share': 0.049444999999999996, 'conversion_share': 0.0}, {'asin': 'B07PJWF7KD', 'click_share': 0.034527999999999996, 'conversion_share': 0.008319}]}, {'x': '2019-04-09', 'y': 39, 'products': [{'asin': 'B07QKMZC9F', 'click_share': 0.086534, 'conversion_share': 0.0}, {'asin': 'B07QFXCL2Y', 'click_share': 0.049444999999999996, 'conversion_share': 0.0}, {'asin': 'B07PJWF7KD', 'click_share': 0.034527999999999996, 'conversion_share': 0.008319}]}, {'x': '2019-04-10', 'y': 93, 'products': [{'asin': 'B07QKMZC9F', 'click_share': 0.086534, 'conversion_share': 0.0}, {'asin': 'B07QFXCL2Y', 'click_share': 0.049444999999999996, 'conversion_share': 0.0}, {'asin': 'B07PJWF7KD', 'click_share': 0.034527999999999996, 'conversion_share': 0.008319}]}, {'x': '2019-04-11', 'y': 89, 'products': [{'asin': 'B07QKMZC9F', 'click_share': 0.086534, 'conversion_share': 0.0}, {'asin': 'B07QFXCL2Y', 'click_share': 0.049444999999999996, 'conversion_share': 0.0}, {'asin': 'B07PJWF7KD', 'click_share': 0.034527999999999996, 'conversion_share': 0.008319}]}, {'x': '2019-04-12', 'y': 50, 'products': [{'asin': 'B07QKMZC9F', 'click_share': 0.086534, 'conversion_share': 0.0}, {'asin': 'B07QFXCL2Y', 'click_share': 0.049444999999999996, 'conversion_share': 0.0}, {'asin': 'B07PJWF7KD', 'click_share': 0.034527999999999996, 'conversion_share': 0.008319}]}, {'x': '2019-04-13', 'y': 6, 'products': [{'asin': 'B07QKMZC9F', 'click_share': 0.086534, 'conversion_share': 0.0}, {'asin': 'B07QFXCL2Y', 'click_share': 0.049444999999999996, 'conversion_share': 0.0}, {'asin': 'B07PJWF7KD', 'click_share': 0.034527999999999996, 'conversion_share': 0.008319}]}, {'x': '2019-04-14', 'y': 42, 'products': [{'asin': 'B07QKMZC9F', 'click_share': 0.086534, 'conversion_share': 0.0}, {'asin': 'B07QFXCL2Y', 'click_share': 0.049444999999999996, 'conversion_share': 0.0}, {'asin': 'B07PJWF7KD', 'click_share': 0.034527999999999996, 'conversion_share': 0.008319}]}, {'x': '2019-04-15', 'y': 99, 'products': [{'asin': 'B07QKMZC9F', 'click_share': 0.086534, 'conversion_share': 0.0}, {'asin': 'B07QFXCL2Y', 'click_share': 0.049444999999999996, 'conversion_share': 0.0}, {'asin': 'B07PJWF7KD', 'click_share': 0.034527999999999996, 'conversion_share': 0.008319}]}, {'x': '2019-04-16', 'y': 21, 'products': [{'asin': 'B07QKMZC9F', 'click_share': 0.086534, 'conversion_share': 0.0}, {'asin': 'B07QFXCL2Y', 'click_share': 0.049444999999999996, 'conversion_share': 0.0}, {'asin': 'B07PJWF7KD', 'click_share': 0.034527999999999996, 'conversion_share': 0.008319}]}, {'x': '2019-04-17', 'y': 72, 'products': [{'asin': 'B07QKMZC9F', 'click_share': 0.086534, 'conversion_share': 0.0}, {'asin': 'B07QFXCL2Y', 'click_share': 0.049444999999999996, 'conversion_share': 0.0}, {'asin': 'B07PJWF7KD', 'click_share': 0.034527999999999996, 'conversion_share': 0.008319}]}, {'x': '2019-04-18', 'y': 82, 'products': [{'asin': 'B07QKMZC9F', 'click_share': 0.086534, 'conversion_share': 0.0}, {'asin': 'B07QFXCL2Y', 'click_share': 0.049444999999999996, 'conversion_share': 0.0}, {'asin': 'B07PJWF7KD', 'click_share': 0.034527999999999996, 'conversion_share': 0.008319}]}, {'x': '2019-04-19', 'y': 55, 'products': [{'asin': 'B07QKMZC9F', 'click_share': 0.086534, 'conversion_share': 0.0}, {'asin': 'B07QFXCL2Y', 'click_share': 0.049444999999999996, 'conversion_share': 0.0}, {'asin': 'B07PJWF7KD', 'click_share': 0.034527999999999996, 'conversion_share': 0.008319}]}, {'x': '2019-04-20', 'y': 2, 'products': [{'asin': 'B07QKMZC9F', 'click_share': 0.086534, 'conversion_share': 0.0}, {'asin': 'B07QFXCL2Y', 'click_share': 0.049444999999999996, 'conversion_share': 0.0}, {'asin': 'B07PJWF7KD', 'click_share': 0.034527999999999996, 'conversion_share': 0.008319}]}, {'x': '2019-04-21', 'y': 57, 'products': [{'asin': 'B07QKMZC9F', 'click_share': 0.086534, 'conversion_share': 0.0}, {'asin': 'B07QFXCL2Y', 'click_share': 0.049444999999999996, 'conversion_share': 0.0}, {'asin': 'B07PJWF7KD', 'click_share': 0.034527999999999996, 'conversion_share': 0.008319}]}, {'x': '2019-04-22', 'y': 44, 'products': [{'asin': 'B07QKMZC9F', 'click_share': 0.086534, 'conversion_share': 0.0}, {'asin': 'B07QFXCL2Y', 'click_share': 0.049444999999999996, 'conversion_share': 0.0}, {'asin': 'B07PJWF7KD', 'click_share': 0.034527999999999996, 'conversion_share': 0.008319}]}, {'x': '2019-04-23', 'y': 78, 'products': [{'asin': 'B07QKMZC9F', 'click_share': 0.086534, 'conversion_share': 0.0}, {'asin': 'B07QFXCL2Y', 'click_share': 0.049444999999999996, 'conversion_share': 0.0}, {'asin': 'B07PJWF7KD', 'click_share': 0.034527999999999996, 'conversion_share': 0.008319}]}, {'x': '2019-04-24', 'y': 67, 'products': [{'asin': 'B07QKMZC9F', 'click_share': 0.086534, 'conversion_share': 0.0}, {'asin': 'B07QFXCL2Y', 'click_share': 0.049444999999999996, 'conversion_share': 0.0}, {'asin': 'B07PJWF7KD', 'click_share': 0.034527999999999996, 'conversion_share': 0.008319}]}, {'x': '2019-04-25', 'y': 8, 'products': [{'asin': 'B07QKMZC9F', 'click_share': 0.086534, 'conversion_share': 0.0}, {'asin': 'B07QFXCL2Y', 'click_share': 0.049444999999999996, 'conversion_share': 0.0}, {'asin': 'B07PJWF7KD', 'click_share': 0.034527999999999996, 'conversion_share': 0.008319}]}, {'x': '2019-04-26', 'y': 54, 'products': [{'asin': 'B07QKMZC9F', 'click_share': 0.086534, 'conversion_share': 0.0}, {'asin': 'B07QFXCL2Y', 'click_share': 0.049444999999999996, 'conversion_share': 0.0}, {'asin': 'B07PJWF7KD', 'click_share': 0.034527999999999996, 'conversion_share': 0.008319}]}, {'x': '2019-04-27', 'y': 3, 'products': [{'asin': 'B07QKMZC9F', 'click_share': 0.086534, 'conversion_share': 0.0}, {'asin': 'B07QFXCL2Y', 'click_share': 0.049444999999999996, 'conversion_share': 0.0}, {'asin': 'B07PJWF7KD', 'click_share': 0.034527999999999996, 'conversion_share': 0.008319}]}, {'x': '2019-04-28', 'y': 62, 'products': [{'asin': 'B07QKMZC9F', 'click_share': 0.086534, 'conversion_share': 0.0}, {'asin': 'B07QFXCL2Y', 'click_share': 0.049444999999999996, 'conversion_share': 0.0}, {'asin': 'B07PJWF7KD', 'click_share': 0.034527999999999996, 'conversion_share': 0.008319}]}, {'x': '2019-04-29', 'y': 11, 'products': [{'asin': 'B07QKMZC9F', 'click_share': 0.086534, 'conversion_share': 0.0}, {'asin': 'B07QFXCL2Y', 'click_share': 0.049444999999999996, 'conversion_share': 0.0}, {'asin': 'B07PJWF7KD', 'click_share': 0.034527999999999996, 'conversion_share': 0.008319}]}, {'x': '2019-04-30', 'y': 100, 'products': [{'asin': 'B07QKMZC9F', 'click_share': 0.086534, 'conversion_share': 0.0}, {'asin': 'B07QFXCL2Y', 'click_share': 0.049444999999999996, 'conversion_share': 0.0}, {'asin': 'B07PJWF7KD', 'click_share': 0.034527999999999996, 'conversion_share': 0.008319}]}]}
        //, {'id': 'bluetooth headphones', 'color': 'hsl(45, 60%, 50%)', 'data': [{'x': '2019-04-01', 'y': 81, 'products': [{'asin': 'B07QKMZC9F', 'click_share': 0.084924, 'conversion_share': 0.0}, {'asin': 'B07QFXCL2Y', 'click_share': 0.040443, 'conversion_share': 0.0}, {'asin': 'B07PQHG76R', 'click_share': 0.037031, 'conversion_share': 0.0}]}, {'x': '2019-04-02', 'y': 2, 'products': [{'asin': 'B07QKMZC9F', 'click_share': 0.084924, 'conversion_share': 0.0}, {'asin': 'B07QFXCL2Y', 'click_share': 0.040443, 'conversion_share': 0.0}, {'asin': 'B07PQHG76R', 'click_share': 0.037031, 'conversion_share': 0.0}]}, {'x': '2019-04-03', 'y': 90, 'products': [{'asin': 'B07QKMZC9F', 'click_share': 0.084924, 'conversion_share': 0.0}, {'asin': 'B07QFXCL2Y', 'click_share': 0.040443, 'conversion_share': 0.0}, {'asin': 'B07PQHG76R', 'click_share': 0.037031, 'conversion_share': 0.0}]}, {'x': '2019-04-04', 'y': 98, 'products': [{'asin': 'B07QKMZC9F', 'click_share': 0.084924, 'conversion_share': 0.0}, {'asin': 'B07QFXCL2Y', 'click_share': 0.040443, 'conversion_share': 0.0}, {'asin': 'B07PQHG76R', 'click_share': 0.037031, 'conversion_share': 0.0}]}, {'x': '2019-04-05', 'y': 51, 'products': [{'asin': 'B07QKMZC9F', 'click_share': 0.084924, 'conversion_share': 0.0}, {'asin': 'B07QFXCL2Y', 'click_share': 0.040443, 'conversion_share': 0.0}, {'asin': 'B07PQHG76R', 'click_share': 0.037031, 'conversion_share': 0.0}]}, {'x': '2019-04-06', 'y': 20, 'products': [{'asin': 'B07QKMZC9F', 'click_share': 0.084924, 'conversion_share': 0.0}, {'asin': 'B07QFXCL2Y', 'click_share': 0.040443, 'conversion_share': 0.0}, {'asin': 'B07PQHG76R', 'click_share': 0.037031, 'conversion_share': 0.0}]}, {'x': '2019-04-07', 'y': 45, 'products': [{'asin': 'B07QKMZC9F', 'click_share': 0.084924, 'conversion_share': 0.0}, {'asin': 'B07QFXCL2Y', 'click_share': 0.040443, 'conversion_share': 0.0}, {'asin': 'B07PQHG76R', 'click_share': 0.037031, 'conversion_share': 0.0}]}, {'x': '2019-04-08', 'y': 9, 'products': [{'asin': 'B07QKMZC9F', 'click_share': 0.084924, 'conversion_share': 0.0}, {'asin': 'B07QFXCL2Y', 'click_share': 0.040443, 'conversion_share': 0.0}, {'asin': 'B07PQHG76R', 'click_share': 0.037031, 'conversion_share': 0.0}]}, {'x': '2019-04-09', 'y': 61, 'products': [{'asin': 'B07QKMZC9F', 'click_share': 0.084924, 'conversion_share': 0.0}, {'asin': 'B07QFXCL2Y', 'click_share': 0.040443, 'conversion_share': 0.0}, {'asin': 'B07PQHG76R', 'click_share': 0.037031, 'conversion_share': 0.0}]}, {'x': '2019-04-10', 'y': 48, 'products': [{'asin': 'B07QKMZC9F', 'click_share': 0.084924, 'conversion_share': 0.0}, {'asin': 'B07QFXCL2Y', 'click_share': 0.040443, 'conversion_share': 0.0}, {'asin': 'B07PQHG76R', 'click_share': 0.037031, 'conversion_share': 0.0}]}, {'x': '2019-04-11', 'y': 21, 'products': [{'asin': 'B07QKMZC9F', 'click_share': 0.084924, 'conversion_share': 0.0}, {'asin': 'B07QFXCL2Y', 'click_share': 0.040443, 'conversion_share': 0.0}, {'asin': 'B07PQHG76R', 'click_share': 0.037031, 'conversion_share': 0.0}]}, {'x': '2019-04-12', 'y': 21, 'products': [{'asin': 'B07QKMZC9F', 'click_share': 0.084924, 'conversion_share': 0.0}, {'asin': 'B07QFXCL2Y', 'click_share': 0.040443, 'conversion_share': 0.0}, {'asin': 'B07PQHG76R', 'click_share': 0.037031, 'conversion_share': 0.0}]}, {'x': '2019-04-13', 'y': 48, 'products': [{'asin': 'B07QKMZC9F', 'click_share': 0.084924, 'conversion_share': 0.0}, {'asin': 'B07QFXCL2Y', 'click_share': 0.040443, 'conversion_share': 0.0}, {'asin': 'B07PQHG76R', 'click_share': 0.037031, 'conversion_share': 0.0}]}, {'x': '2019-04-14', 'y': 26, 'products': [{'asin': 'B07QKMZC9F', 'click_share': 0.084924, 'conversion_share': 0.0}, {'asin': 'B07QFXCL2Y', 'click_share': 0.040443, 'conversion_share': 0.0}, {'asin': 'B07PQHG76R', 'click_share': 0.037031, 'conversion_share': 0.0}]}, {'x': '2019-04-15', 'y': 68, 'products': [{'asin': 'B07QKMZC9F', 'click_share': 0.084924, 'conversion_share': 0.0}, {'asin': 'B07QFXCL2Y', 'click_share': 0.040443, 'conversion_share': 0.0}, {'asin': 'B07PQHG76R', 'click_share': 0.037031, 'conversion_share': 0.0}]}, {'x': '2019-04-16', 'y': 45, 'products': [{'asin': 'B07QKMZC9F', 'click_share': 0.084924, 'conversion_share': 0.0}, {'asin': 'B07QFXCL2Y', 'click_share': 0.040443, 'conversion_share': 0.0}, {'asin': 'B07PQHG76R', 'click_share': 0.037031, 'conversion_share': 0.0}]}, {'x': '2019-04-17', 'y': 39, 'products': [{'asin': 'B07QKMZC9F', 'click_share': 0.084924, 'conversion_share': 0.0}, {'asin': 'B07QFXCL2Y', 'click_share': 0.040443, 'conversion_share': 0.0}, {'asin': 'B07PQHG76R', 'click_share': 0.037031, 'conversion_share': 0.0}]}, {'x': '2019-04-18', 'y': 84, 'products': [{'asin': 'B07QKMZC9F', 'click_share': 0.084924, 'conversion_share': 0.0}, {'asin': 'B07QFXCL2Y', 'click_share': 0.040443, 'conversion_share': 0.0}, {'asin': 'B07PQHG76R', 'click_share': 0.037031, 'conversion_share': 0.0}]}, {'x': '2019-04-19', 'y': 41, 'products': [{'asin': 'B07QKMZC9F', 'click_share': 0.084924, 'conversion_share': 0.0}, {'asin': 'B07QFXCL2Y', 'click_share': 0.040443, 'conversion_share': 0.0}, {'asin': 'B07PQHG76R', 'click_share': 0.037031, 'conversion_share': 0.0}]}, {'x': '2019-04-20', 'y': 14, 'products': [{'asin': 'B07QKMZC9F', 'click_share': 0.084924, 'conversion_share': 0.0}, {'asin': 'B07QFXCL2Y', 'click_share': 0.040443, 'conversion_share': 0.0}, {'asin': 'B07PQHG76R', 'click_share': 0.037031, 'conversion_share': 0.0}]}, {'x': '2019-04-21', 'y': 72, 'products': [{'asin': 'B07QKMZC9F', 'click_share': 0.084924, 'conversion_share': 0.0}, {'asin': 'B07QFXCL2Y', 'click_share': 0.040443, 'conversion_share': 0.0}, {'asin': 'B07PQHG76R', 'click_share': 0.037031, 'conversion_share': 0.0}]}, {'x': '2019-04-22', 'y': 10, 'products': [{'asin': 'B07QKMZC9F', 'click_share': 0.084924, 'conversion_share': 0.0}, {'asin': 'B07QFXCL2Y', 'click_share': 0.040443, 'conversion_share': 0.0}, {'asin': 'B07PQHG76R', 'click_share': 0.037031, 'conversion_share': 0.0}]}, {'x': '2019-04-23', 'y': 15, 'products': [{'asin': 'B07QKMZC9F', 'click_share': 0.084924, 'conversion_share': 0.0}, {'asin': 'B07QFXCL2Y', 'click_share': 0.040443, 'conversion_share': 0.0}, {'asin': 'B07PQHG76R', 'click_share': 0.037031, 'conversion_share': 0.0}]}, {'x': '2019-04-24', 'y': 53, 'products': [{'asin': 'B07QKMZC9F', 'click_share': 0.084924, 'conversion_share': 0.0}, {'asin': 'B07QFXCL2Y', 'click_share': 0.040443, 'conversion_share': 0.0}, {'asin': 'B07PQHG76R', 'click_share': 0.037031, 'conversion_share': 0.0}]}, {'x': '2019-04-25', 'y': 3, 'products': [{'asin': 'B07QKMZC9F', 'click_share': 0.084924, 'conversion_share': 0.0}, {'asin': 'B07QFXCL2Y', 'click_share': 0.040443, 'conversion_share': 0.0}, {'asin': 'B07PQHG76R', 'click_share': 0.037031, 'conversion_share': 0.0}]}, {'x': '2019-04-26', 'y': 48, 'products': [{'asin': 'B07QKMZC9F', 'click_share': 0.084924, 'conversion_share': 0.0}, {'asin': 'B07QFXCL2Y', 'click_share': 0.040443, 'conversion_share': 0.0}, {'asin': 'B07PQHG76R', 'click_share': 0.037031, 'conversion_share': 0.0}]}, {'x': '2019-04-27', 'y': 52, 'products': [{'asin': 'B07QKMZC9F', 'click_share': 0.084924, 'conversion_share': 0.0}, {'asin': 'B07QFXCL2Y', 'click_share': 0.040443, 'conversion_share': 0.0}, {'asin': 'B07PQHG76R', 'click_share': 0.037031, 'conversion_share': 0.0}]}, {'x': '2019-04-28', 'y': 82, 'products': [{'asin': 'B07QKMZC9F', 'click_share': 0.084924, 'conversion_share': 0.0}, {'asin': 'B07QFXCL2Y', 'click_share': 0.040443, 'conversion_share': 0.0}, {'asin': 'B07PQHG76R', 'click_share': 0.037031, 'conversion_share': 0.0}]}, {'x': '2019-04-29', 'y': 64, 'products': [{'asin': 'B07QKMZC9F', 'click_share': 0.084924, 'conversion_share': 0.0}, {'asin': 'B07QFXCL2Y', 'click_share': 0.040443, 'conversion_share': 0.0}, {'asin': 'B07PQHG76R', 'click_share': 0.037031, 'conversion_share': 0.0}]}, {'x': '2019-04-30', 'y': 10, 'products': [{'asin': 'B07QKMZC9F', 'click_share': 0.084924, 'conversion_share': 0.0}, {'asin': 'B07QFXCL2Y', 'click_share': 0.040443, 'conversion_share': 0.0}, {'asin': 'B07PQHG76R', 'click_share': 0.037031, 'conversion_share': 0.0}]}]}, {'id': 'iphone charger', 'color': 'hsl(115, 70%, 50%)', 'data': [{'x': '2019-04-01', 'y': 60, 'products': [{'asin': 'B07R4DYR5H', 'click_share': 0.067557, 'conversion_share': 0.0}, {'asin': 'B07QXF8RDV', 'click_share': 0.062646, 'conversion_share': 0.000211}, {'asin': 'B07R1JZYFM', 'click_share': 0.051125, 'conversion_share': 0.0}]}, {'x': '2019-04-02', 'y': 43, 'products': [{'asin': 'B07R4DYR5H', 'click_share': 0.067557, 'conversion_share': 0.0}, {'asin': 'B07QXF8RDV', 'click_share': 0.062646, 'conversion_share': 0.000211}, {'asin': 'B07R1JZYFM', 'click_share': 0.051125, 'conversion_share': 0.0}]}, {'x': '2019-04-03', 'y': 23, 'products': [{'asin': 'B07R4DYR5H', 'click_share': 0.067557, 'conversion_share': 0.0}, {'asin': 'B07QXF8RDV', 'click_share': 0.062646, 'conversion_share': 0.000211}, {'asin': 'B07R1JZYFM', 'click_share': 0.051125, 'conversion_share': 0.0}]}, {'x': '2019-04-04', 'y': 2, 'products': [{'asin': 'B07R4DYR5H', 'click_share': 0.067557, 'conversion_share': 0.0}, {'asin': 'B07QXF8RDV', 'click_share': 0.062646, 'conversion_share': 0.000211}, {'asin': 'B07R1JZYFM', 'click_share': 0.051125, 'conversion_share': 0.0}]}, {'x': '2019-04-05', 'y': 74, 'products': [{'asin': 'B07R4DYR5H', 'click_share': 0.067557, 'conversion_share': 0.0}, {'asin': 'B07QXF8RDV', 'click_share': 0.062646, 'conversion_share': 0.000211}, {'asin': 'B07R1JZYFM', 'click_share': 0.051125, 'conversion_share': 0.0}]}, {'x': '2019-04-06', 'y': 43, 'products': [{'asin': 'B07R4DYR5H', 'click_share': 0.067557, 'conversion_share': 0.0}, {'asin': 'B07QXF8RDV', 'click_share': 0.062646, 'conversion_share': 0.000211}, {'asin': 'B07R1JZYFM', 'click_share': 0.051125, 'conversion_share': 0.0}]}, {'x': '2019-04-07', 'y': 91, 'products': [{'asin': 'B07R4DYR5H', 'click_share': 0.067557, 'conversion_share': 0.0}, {'asin': 'B07QXF8RDV', 'click_share': 0.062646, 'conversion_share': 0.000211}, {'asin': 'B07R1JZYFM', 'click_share': 0.051125, 'conversion_share': 0.0}]}, {'x': '2019-04-08', 'y': 81, 'products': [{'asin': 'B07R4DYR5H', 'click_share': 0.067557, 'conversion_share': 0.0}, {'asin': 'B07QXF8RDV', 'click_share': 0.062646, 'conversion_share': 0.000211}, {'asin': 'B07R1JZYFM', 'click_share': 0.051125, 'conversion_share': 0.0}]}, {'x': '2019-04-09', 'y': 37, 'products': [{'asin': 'B07R4DYR5H', 'click_share': 0.067557, 'conversion_share': 0.0}, {'asin': 'B07QXF8RDV', 'click_share': 0.062646, 'conversion_share': 0.000211}, {'asin': 'B07R1JZYFM', 'click_share': 0.051125, 'conversion_share': 0.0}]}, {'x': '2019-04-10', 'y': 22, 'products': [{'asin': 'B07R4DYR5H', 'click_share': 0.067557, 'conversion_share': 0.0}, {'asin': 'B07QXF8RDV', 'click_share': 0.062646, 'conversion_share': 0.000211}, {'asin': 'B07R1JZYFM', 'click_share': 0.051125, 'conversion_share': 0.0}]}, {'x': '2019-04-11', 'y': 67, 'products': [{'asin': 'B07R4DYR5H', 'click_share': 0.067557, 'conversion_share': 0.0}, {'asin': 'B07QXF8RDV', 'click_share': 0.062646, 'conversion_share': 0.000211}, {'asin': 'B07R1JZYFM', 'click_share': 0.051125, 'conversion_share': 0.0}]}, {'x': '2019-04-12', 'y': 64, 'products': [{'asin': 'B07R4DYR5H', 'click_share': 0.067557, 'conversion_share': 0.0}, {'asin': 'B07QXF8RDV', 'click_share': 0.062646, 'conversion_share': 0.000211}, {'asin': 'B07R1JZYFM', 'click_share': 0.051125, 'conversion_share': 0.0}]}, {'x': '2019-04-13', 'y': 73, 'products': [{'asin': 'B07R4DYR5H', 'click_share': 0.067557, 'conversion_share': 0.0}, {'asin': 'B07QXF8RDV', 'click_share': 0.062646, 'conversion_share': 0.000211}, {'asin': 'B07R1JZYFM', 'click_share': 0.051125, 'conversion_share': 0.0}]}, {'x': '2019-04-14', 'y': 24, 'products': [{'asin': 'B07R4DYR5H', 'click_share': 0.067557, 'conversion_share': 0.0}, {'asin': 'B07QXF8RDV', 'click_share': 0.062646, 'conversion_share': 0.000211}, {'asin': 'B07R1JZYFM', 'click_share': 0.051125, 'conversion_share': 0.0}]}, {'x': '2019-04-15', 'y': 86, 'products': [{'asin': 'B07R4DYR5H', 'click_share': 0.067557, 'conversion_share': 0.0}, {'asin': 'B07QXF8RDV', 'click_share': 0.062646, 'conversion_share': 0.000211}, {'asin': 'B07R1JZYFM', 'click_share': 0.051125, 'conversion_share': 0.0}]}, {'x': '2019-04-16', 'y': 76, 'products': [{'asin': 'B07R4DYR5H', 'click_share': 0.067557, 'conversion_share': 0.0}, {'asin': 'B07QXF8RDV', 'click_share': 0.062646, 'conversion_share': 0.000211}, {'asin': 'B07R1JZYFM', 'click_share': 0.051125, 'conversion_share': 0.0}]}, {'x': '2019-04-17', 'y': 53, 'products': [{'asin': 'B07R4DYR5H', 'click_share': 0.067557, 'conversion_share': 0.0}, {'asin': 'B07QXF8RDV', 'click_share': 0.062646, 'conversion_share': 0.000211}, {'asin': 'B07R1JZYFM', 'click_share': 0.051125, 'conversion_share': 0.0}]}, {'x': '2019-04-18', 'y': 64, 'products': [{'asin': 'B07R4DYR5H', 'click_share': 0.067557, 'conversion_share': 0.0}, {'asin': 'B07QXF8RDV', 'click_share': 0.062646, 'conversion_share': 0.000211}, {'asin': 'B07R1JZYFM', 'click_share': 0.051125, 'conversion_share': 0.0}]}, {'x': '2019-04-19', 'y': 98, 'products': [{'asin': 'B07R4DYR5H', 'click_share': 0.067557, 'conversion_share': 0.0}, {'asin': 'B07QXF8RDV', 'click_share': 0.062646, 'conversion_share': 0.000211}, {'asin': 'B07R1JZYFM', 'click_share': 0.051125, 'conversion_share': 0.0}]}, {'x': '2019-04-20', 'y': 35, 'products': [{'asin': 'B07R4DYR5H', 'click_share': 0.067557, 'conversion_share': 0.0}, {'asin': 'B07QXF8RDV', 'click_share': 0.062646, 'conversion_share': 0.000211}, {'asin': 'B07R1JZYFM', 'click_share': 0.051125, 'conversion_share': 0.0}]}, {'x': '2019-04-21', 'y': 96, 'products': [{'asin': 'B07R4DYR5H', 'click_share': 0.067557, 'conversion_share': 0.0}, {'asin': 'B07QXF8RDV', 'click_share': 0.062646, 'conversion_share': 0.000211}, {'asin': 'B07R1JZYFM', 'click_share': 0.051125, 'conversion_share': 0.0}]}, {'x': '2019-04-22', 'y': 100, 'products': [{'asin': 'B07R4DYR5H', 'click_share': 0.067557, 'conversion_share': 0.0}, {'asin': 'B07QXF8RDV', 'click_share': 0.062646, 'conversion_share': 0.000211}, {'asin': 'B07R1JZYFM', 'click_share': 0.051125, 'conversion_share': 0.0}]}, {'x': '2019-04-23', 'y': 65, 'products': [{'asin': 'B07R4DYR5H', 'click_share': 0.067557, 'conversion_share': 0.0}, {'asin': 'B07QXF8RDV', 'click_share': 0.062646, 'conversion_share': 0.000211}, {'asin': 'B07R1JZYFM', 'click_share': 0.051125, 'conversion_share': 0.0}]}, {'x': '2019-04-24', 'y': 100, 'products': [{'asin': 'B07R4DYR5H', 'click_share': 0.067557, 'conversion_share': 0.0}, {'asin': 'B07QXF8RDV', 'click_share': 0.062646, 'conversion_share': 0.000211}, {'asin': 'B07R1JZYFM', 'click_share': 0.051125, 'conversion_share': 0.0}]}, {'x': '2019-04-25', 'y': 17, 'products': [{'asin': 'B07R4DYR5H', 'click_share': 0.067557, 'conversion_share': 0.0}, {'asin': 'B07QXF8RDV', 'click_share': 0.062646, 'conversion_share': 0.000211}, {'asin': 'B07R1JZYFM', 'click_share': 0.051125, 'conversion_share': 0.0}]}, {'x': '2019-04-26', 'y': 24, 'products': [{'asin': 'B07R4DYR5H', 'click_share': 0.067557, 'conversion_share': 0.0}, {'asin': 'B07QXF8RDV', 'click_share': 0.062646, 'conversion_share': 0.000211}, {'asin': 'B07R1JZYFM', 'click_share': 0.051125, 'conversion_share': 0.0}]}, {'x': '2019-04-27', 'y': 39, 'products': [{'asin': 'B07R4DYR5H', 'click_share': 0.067557, 'conversion_share': 0.0}, {'asin': 'B07QXF8RDV', 'click_share': 0.062646, 'conversion_share': 0.000211}, {'asin': 'B07R1JZYFM', 'click_share': 0.051125, 'conversion_share': 0.0}]}, {'x': '2019-04-28', 'y': 52, 'products': [{'asin': 'B07R4DYR5H', 'click_share': 0.067557, 'conversion_share': 0.0}, {'asin': 'B07QXF8RDV', 'click_share': 0.062646, 'conversion_share': 0.000211}, {'asin': 'B07R1JZYFM', 'click_share': 0.051125, 'conversion_share': 0.0}]}, {'x': '2019-04-29', 'y': 57, 'products': [{'asin': 'B07R4DYR5H', 'click_share': 0.067557, 'conversion_share': 0.0}, {'asin': 'B07QXF8RDV', 'click_share': 0.062646, 'conversion_share': 0.000211}, {'asin': 'B07R1JZYFM', 'click_share': 0.051125, 'conversion_share': 0.0}]}, {'x': '2019-04-30', 'y': 67, 'products': [{'asin': 'B07R4DYR5H', 'click_share': 0.067557, 'conversion_share': 0.0}, {'asin': 'B07QXF8RDV', 'click_share': 0.062646, 'conversion_share': 0.000211}, {'asin': 'B07R1JZYFM', 'click_share': 0.051125, 'conversion_share': 0.0}]}]}, {'id': 'airpods', 'color': 'hsl(302, 70%, 50%)', 'data': [{'x': '2019-04-01', 'y': 56, 'products': [{'asin': 'B07PXGQC1Q', 'click_share': 0.194163, 'conversion_share': 0.3932}, {'asin': 'B01MQWUXZS', 'click_share': 0.061772, 'conversion_share': 0.199651}, {'asin': 'B07QTJ3G88', 'click_share': 0.029324, 'conversion_share': 0.031386}]}, {'x': '2019-04-02', 'y': 15, 'products': [{'asin': 'B07PXGQC1Q', 'click_share': 0.194163, 'conversion_share': 0.3932}, {'asin': 'B01MQWUXZS', 'click_share': 0.061772, 'conversion_share': 0.199651}, {'asin': 'B07QTJ3G88', 'click_share': 0.029324, 'conversion_share': 0.031386}]}, {'x': '2019-04-03', 'y': 63, 'products': [{'asin': 'B07PXGQC1Q', 'click_share': 0.194163, 'conversion_share': 0.3932}, {'asin': 'B01MQWUXZS', 'click_share': 0.061772, 'conversion_share': 0.199651}, {'asin': 'B07QTJ3G88', 'click_share': 0.029324, 'conversion_share': 0.031386}]}, {'x': '2019-04-04', 'y': 59, 'products': [{'asin': 'B07PXGQC1Q', 'click_share': 0.194163, 'conversion_share': 0.3932}, {'asin': 'B01MQWUXZS', 'click_share': 0.061772, 'conversion_share': 0.199651}, {'asin': 'B07QTJ3G88', 'click_share': 0.029324, 'conversion_share': 0.031386}]}, {'x': '2019-04-05', 'y': 80, 'products': [{'asin': 'B07PXGQC1Q', 'click_share': 0.194163, 'conversion_share': 0.3932}, {'asin': 'B01MQWUXZS', 'click_share': 0.061772, 'conversion_share': 0.199651}, {'asin': 'B07QTJ3G88', 'click_share': 0.029324, 'conversion_share': 0.031386}]}, {'x': '2019-04-06', 'y': 91, 'products': [{'asin': 'B07PXGQC1Q', 'click_share': 0.194163, 'conversion_share': 0.3932}, {'asin': 'B01MQWUXZS', 'click_share': 0.061772, 'conversion_share': 0.199651}, {'asin': 'B07QTJ3G88', 'click_share': 0.029324, 'conversion_share': 0.031386}]}, {'x': '2019-04-07', 'y': 84, 'products': [{'asin': 'B07PXGQC1Q', 'click_share': 0.194163, 'conversion_share': 0.3932}, {'asin': 'B01MQWUXZS', 'click_share': 0.061772, 'conversion_share': 0.199651}, {'asin': 'B07QTJ3G88', 'click_share': 0.029324, 'conversion_share': 0.031386}]}, {'x': '2019-04-08', 'y': 53, 'products': [{'asin': 'B07PXGQC1Q', 'click_share': 0.194163, 'conversion_share': 0.3932}, {'asin': 'B01MQWUXZS', 'click_share': 0.061772, 'conversion_share': 0.199651}, {'asin': 'B07QTJ3G88', 'click_share': 0.029324, 'conversion_share': 0.031386}]}, {'x': '2019-04-09', 'y': 35, 'products': [{'asin': 'B07PXGQC1Q', 'click_share': 0.194163, 'conversion_share': 0.3932}, {'asin': 'B01MQWUXZS', 'click_share': 0.061772, 'conversion_share': 0.199651}, {'asin': 'B07QTJ3G88', 'click_share': 0.029324, 'conversion_share': 0.031386}]}, {'x': '2019-04-10', 'y': 8, 'products': [{'asin': 'B07PXGQC1Q', 'click_share': 0.194163, 'conversion_share': 0.3932}, {'asin': 'B01MQWUXZS', 'click_share': 0.061772, 'conversion_share': 0.199651}, {'asin': 'B07QTJ3G88', 'click_share': 0.029324, 'conversion_share': 0.031386}]}, {'x': '2019-04-11', 'y': 4, 'products': [{'asin': 'B07PXGQC1Q', 'click_share': 0.194163, 'conversion_share': 0.3932}, {'asin': 'B01MQWUXZS', 'click_share': 0.061772, 'conversion_share': 0.199651}, {'asin': 'B07QTJ3G88', 'click_share': 0.029324, 'conversion_share': 0.031386}]}, {'x': '2019-04-12', 'y': 55, 'products': [{'asin': 'B07PXGQC1Q', 'click_share': 0.194163, 'conversion_share': 0.3932}, {'asin': 'B01MQWUXZS', 'click_share': 0.061772, 'conversion_share': 0.199651}, {'asin': 'B07QTJ3G88', 'click_share': 0.029324, 'conversion_share': 0.031386}]}, {'x': '2019-04-13', 'y': 33, 'products': [{'asin': 'B07PXGQC1Q', 'click_share': 0.194163, 'conversion_share': 0.3932}, {'asin': 'B01MQWUXZS', 'click_share': 0.061772, 'conversion_share': 0.199651}, {'asin': 'B07QTJ3G88', 'click_share': 0.029324, 'conversion_share': 0.031386}]}, {'x': '2019-04-14', 'y': 98, 'products': [{'asin': 'B07PXGQC1Q', 'click_share': 0.194163, 'conversion_share': 0.3932}, {'asin': 'B01MQWUXZS', 'click_share': 0.061772, 'conversion_share': 0.199651}, {'asin': 'B07QTJ3G88', 'click_share': 0.029324, 'conversion_share': 0.031386}]}, {'x': '2019-04-15', 'y': 86, 'products': [{'asin': 'B07PXGQC1Q', 'click_share': 0.194163, 'conversion_share': 0.3932}, {'asin': 'B01MQWUXZS', 'click_share': 0.061772, 'conversion_share': 0.199651}, {'asin': 'B07QTJ3G88', 'click_share': 0.029324, 'conversion_share': 0.031386}]}, {'x': '2019-04-16', 'y': 48, 'products': [{'asin': 'B07PXGQC1Q', 'click_share': 0.194163, 'conversion_share': 0.3932}, {'asin': 'B01MQWUXZS', 'click_share': 0.061772, 'conversion_share': 0.199651}, {'asin': 'B07QTJ3G88', 'click_share': 0.029324, 'conversion_share': 0.031386}]}, {'x': '2019-04-17', 'y': 60, 'products': [{'asin': 'B07PXGQC1Q', 'click_share': 0.194163, 'conversion_share': 0.3932}, {'asin': 'B01MQWUXZS', 'click_share': 0.061772, 'conversion_share': 0.199651}, {'asin': 'B07QTJ3G88', 'click_share': 0.029324, 'conversion_share': 0.031386}]}, {'x': '2019-04-18', 'y': 87, 'products': [{'asin': 'B07PXGQC1Q', 'click_share': 0.194163, 'conversion_share': 0.3932}, {'asin': 'B01MQWUXZS', 'click_share': 0.061772, 'conversion_share': 0.199651}, {'asin': 'B07QTJ3G88', 'click_share': 0.029324, 'conversion_share': 0.031386}]}, {'x': '2019-04-19', 'y': 35, 'products': [{'asin': 'B07PXGQC1Q', 'click_share': 0.194163, 'conversion_share': 0.3932}, {'asin': 'B01MQWUXZS', 'click_share': 0.061772, 'conversion_share': 0.199651}, {'asin': 'B07QTJ3G88', 'click_share': 0.029324, 'conversion_share': 0.031386}]}, {'x': '2019-04-20', 'y': 97, 'products': [{'asin': 'B07PXGQC1Q', 'click_share': 0.194163, 'conversion_share': 0.3932}, {'asin': 'B01MQWUXZS', 'click_share': 0.061772, 'conversion_share': 0.199651}, {'asin': 'B07QTJ3G88', 'click_share': 0.029324, 'conversion_share': 0.031386}]}, {'x': '2019-04-21', 'y': 62, 'products': [{'asin': 'B07PXGQC1Q', 'click_share': 0.194163, 'conversion_share': 0.3932}, {'asin': 'B01MQWUXZS', 'click_share': 0.061772, 'conversion_share': 0.199651}, {'asin': 'B07QTJ3G88', 'click_share': 0.029324, 'conversion_share': 0.031386}]}, {'x': '2019-04-22', 'y': 94, 'products': [{'asin': 'B07PXGQC1Q', 'click_share': 0.194163, 'conversion_share': 0.3932}, {'asin': 'B01MQWUXZS', 'click_share': 0.061772, 'conversion_share': 0.199651}, {'asin': 'B07QTJ3G88', 'click_share': 0.029324, 'conversion_share': 0.031386}]}, {'x': '2019-04-23', 'y': 100, 'products': [{'asin': 'B07PXGQC1Q', 'click_share': 0.194163, 'conversion_share': 0.3932}, {'asin': 'B01MQWUXZS', 'click_share': 0.061772, 'conversion_share': 0.199651}, {'asin': 'B07QTJ3G88', 'click_share': 0.029324, 'conversion_share': 0.031386}]}, {'x': '2019-04-24', 'y': 65, 'products': [{'asin': 'B07PXGQC1Q', 'click_share': 0.194163, 'conversion_share': 0.3932}, {'asin': 'B01MQWUXZS', 'click_share': 0.061772, 'conversion_share': 0.199651}, {'asin': 'B07QTJ3G88', 'click_share': 0.029324, 'conversion_share': 0.031386}]}, {'x': '2019-04-25', 'y': 52, 'products': [{'asin': 'B07PXGQC1Q', 'click_share': 0.194163, 'conversion_share': 0.3932}, {'asin': 'B01MQWUXZS', 'click_share': 0.061772, 'conversion_share': 0.199651}, {'asin': 'B07QTJ3G88', 'click_share': 0.029324, 'conversion_share': 0.031386}]}, {'x': '2019-04-26', 'y': 45, 'products': [{'asin': 'B07PXGQC1Q', 'click_share': 0.194163, 'conversion_share': 0.3932}, {'asin': 'B01MQWUXZS', 'click_share': 0.061772, 'conversion_share': 0.199651}, {'asin': 'B07QTJ3G88', 'click_share': 0.029324, 'conversion_share': 0.031386}]}, {'x': '2019-04-27', 'y': 47, 'products': [{'asin': 'B07PXGQC1Q', 'click_share': 0.194163, 'conversion_share': 0.3932}, {'asin': 'B01MQWUXZS', 'click_share': 0.061772, 'conversion_share': 0.199651}, {'asin': 'B07QTJ3G88', 'click_share': 0.029324, 'conversion_share': 0.031386}]}, {'x': '2019-04-28', 'y': 60, 'products': [{'asin': 'B07PXGQC1Q', 'click_share': 0.194163, 'conversion_share': 0.3932}, {'asin': 'B01MQWUXZS', 'click_share': 0.061772, 'conversion_share': 0.199651}, {'asin': 'B07QTJ3G88', 'click_share': 0.029324, 'conversion_share': 0.031386}]}, {'x': '2019-04-29', 'y': 47, 'products': [{'asin': 'B07PXGQC1Q', 'click_share': 0.194163, 'conversion_share': 0.3932}, {'asin': 'B01MQWUXZS', 'click_share': 0.061772, 'conversion_share': 0.199651}, {'asin': 'B07QTJ3G88', 'click_share': 0.029324, 'conversion_share': 0.031386}]}, {'x': '2019-04-30', 'y': 39, 'products': [{'asin': 'B07PXGQC1Q', 'click_share': 0.194163, 'conversion_share': 0.3932}, {'asin': 'B01MQWUXZS', 'click_share': 0.061772, 'conversion_share': 0.199651}, {'asin': 'B07QTJ3G88', 'click_share': 0.029324, 'conversion_share': 0.031386}]}]}, {'id': 'game of thrones', 'color': 'hsl(69, 70%, 50%)', 'data': [{'x': '2019-04-01', 'y': 43, 'products': [{'asin': 'B07KGL9531', 'click_share': 0.267766, 'conversion_share': 0.020376}, {'asin': 'B007BVMVDU', 'click_share': 0.177746, 'conversion_share': 0.18181799999999998}, {'asin': 'B007HJ84ZK', 'click_share': 0.08209, 'conversion_share': 0.00627}]}, {'x': '2019-04-02', 'y': 29, 'products': [{'asin': 'B07KGL9531', 'click_share': 0.267766, 'conversion_share': 0.020376}, {'asin': 'B007BVMVDU', 'click_share': 0.177746, 'conversion_share': 0.18181799999999998}, {'asin': 'B007HJ84ZK', 'click_share': 0.08209, 'conversion_share': 0.00627}]}, {'x': '2019-04-03', 'y': 50, 'products': [{'asin': 'B07KGL9531', 'click_share': 0.267766, 'conversion_share': 0.020376}, {'asin': 'B007BVMVDU', 'click_share': 0.177746, 'conversion_share': 0.18181799999999998}, {'asin': 'B007HJ84ZK', 'click_share': 0.08209, 'conversion_share': 0.00627}]}, {'x': '2019-04-04', 'y': 53, 'products': [{'asin': 'B07KGL9531', 'click_share': 0.267766, 'conversion_share': 0.020376}, {'asin': 'B007BVMVDU', 'click_share': 0.177746, 'conversion_share': 0.18181799999999998}, {'asin': 'B007HJ84ZK', 'click_share': 0.08209, 'conversion_share': 0.00627}]}, {'x': '2019-04-05', 'y': 73, 'products': [{'asin': 'B07KGL9531', 'click_share': 0.267766, 'conversion_share': 0.020376}, {'asin': 'B007BVMVDU', 'click_share': 0.177746, 'conversion_share': 0.18181799999999998}, {'asin': 'B007HJ84ZK', 'click_share': 0.08209, 'conversion_share': 0.00627}]}, {'x': '2019-04-06', 'y': 20, 'products': [{'asin': 'B07KGL9531', 'click_share': 0.267766, 'conversion_share': 0.020376}, {'asin': 'B007BVMVDU', 'click_share': 0.177746, 'conversion_share': 0.18181799999999998}, {'asin': 'B007HJ84ZK', 'click_share': 0.08209, 'conversion_share': 0.00627}]}, {'x': '2019-04-07', 'y': 80, 'products': [{'asin': 'B07KGL9531', 'click_share': 0.267766, 'conversion_share': 0.020376}, {'asin': 'B007BVMVDU', 'click_share': 0.177746, 'conversion_share': 0.18181799999999998}, {'asin': 'B007HJ84ZK', 'click_share': 0.08209, 'conversion_share': 0.00627}]}, {'x': '2019-04-08', 'y': 11, 'products': [{'asin': 'B07KGL9531', 'click_share': 0.267766, 'conversion_share': 0.020376}, {'asin': 'B007BVMVDU', 'click_share': 0.177746, 'conversion_share': 0.18181799999999998}, {'asin': 'B007HJ84ZK', 'click_share': 0.08209, 'conversion_share': 0.00627}]}, {'x': '2019-04-09', 'y': 10, 'products': [{'asin': 'B07KGL9531', 'click_share': 0.267766, 'conversion_share': 0.020376}, {'asin': 'B007BVMVDU', 'click_share': 0.177746, 'conversion_share': 0.18181799999999998}, {'asin': 'B007HJ84ZK', 'click_share': 0.08209, 'conversion_share': 0.00627}]}, {'x': '2019-04-10', 'y': 97, 'products': [{'asin': 'B07KGL9531', 'click_share': 0.267766, 'conversion_share': 0.020376}, {'asin': 'B007BVMVDU', 'click_share': 0.177746, 'conversion_share': 0.18181799999999998}, {'asin': 'B007HJ84ZK', 'click_share': 0.08209, 'conversion_share': 0.00627}]}, {'x': '2019-04-11', 'y': 10, 'products': [{'asin': 'B07KGL9531', 'click_share': 0.267766, 'conversion_share': 0.020376}, {'asin': 'B007BVMVDU', 'click_share': 0.177746, 'conversion_share': 0.18181799999999998}, {'asin': 'B007HJ84ZK', 'click_share': 0.08209, 'conversion_share': 0.00627}]}, {'x': '2019-04-12', 'y': 88, 'products': [{'asin': 'B07KGL9531', 'click_share': 0.267766, 'conversion_share': 0.020376}, {'asin': 'B007BVMVDU', 'click_share': 0.177746, 'conversion_share': 0.18181799999999998}, {'asin': 'B007HJ84ZK', 'click_share': 0.08209, 'conversion_share': 0.00627}]}, {'x': '2019-04-13', 'y': 44, 'products': [{'asin': 'B07KGL9531', 'click_share': 0.267766, 'conversion_share': 0.020376}, {'asin': 'B007BVMVDU', 'click_share': 0.177746, 'conversion_share': 0.18181799999999998}, {'asin': 'B007HJ84ZK', 'click_share': 0.08209, 'conversion_share': 0.00627}]}, {'x': '2019-04-14', 'y': 49, 'products': [{'asin': 'B07KGL9531', 'click_share': 0.267766, 'conversion_share': 0.020376}, {'asin': 'B007BVMVDU', 'click_share': 0.177746, 'conversion_share': 0.18181799999999998}, {'asin': 'B007HJ84ZK', 'click_share': 0.08209, 'conversion_share': 0.00627}]}, {'x': '2019-04-15', 'y': 42, 'products': [{'asin': 'B07KGL9531', 'click_share': 0.267766, 'conversion_share': 0.020376}, {'asin': 'B007BVMVDU', 'click_share': 0.177746, 'conversion_share': 0.18181799999999998}, {'asin': 'B007HJ84ZK', 'click_share': 0.08209, 'conversion_share': 0.00627}]}, {'x': '2019-04-16', 'y': 35, 'products': [{'asin': 'B07KGL9531', 'click_share': 0.267766, 'conversion_share': 0.020376}, {'asin': 'B007BVMVDU', 'click_share': 0.177746, 'conversion_share': 0.18181799999999998}, {'asin': 'B007HJ84ZK', 'click_share': 0.08209, 'conversion_share': 0.00627}]}, {'x': '2019-04-17', 'y': 66, 'products': [{'asin': 'B07KGL9531', 'click_share': 0.267766, 'conversion_share': 0.020376}, {'asin': 'B007BVMVDU', 'click_share': 0.177746, 'conversion_share': 0.18181799999999998}, {'asin': 'B007HJ84ZK', 'click_share': 0.08209, 'conversion_share': 0.00627}]}, {'x': '2019-04-18', 'y': 24, 'products': [{'asin': 'B07KGL9531', 'click_share': 0.267766, 'conversion_share': 0.020376}, {'asin': 'B007BVMVDU', 'click_share': 0.177746, 'conversion_share': 0.18181799999999998}, {'asin': 'B007HJ84ZK', 'click_share': 0.08209, 'conversion_share': 0.00627}]}, {'x': '2019-04-19', 'y': 79, 'products': [{'asin': 'B07KGL9531', 'click_share': 0.267766, 'conversion_share': 0.020376}, {'asin': 'B007BVMVDU', 'click_share': 0.177746, 'conversion_share': 0.18181799999999998}, {'asin': 'B007HJ84ZK', 'click_share': 0.08209, 'conversion_share': 0.00627}]}, {'x': '2019-04-20', 'y': 28, 'products': [{'asin': 'B07KGL9531', 'click_share': 0.267766, 'conversion_share': 0.020376}, {'asin': 'B007BVMVDU', 'click_share': 0.177746, 'conversion_share': 0.18181799999999998}, {'asin': 'B007HJ84ZK', 'click_share': 0.08209, 'conversion_share': 0.00627}]}, {'x': '2019-04-21', 'y': 36, 'products': [{'asin': 'B07KGL9531', 'click_share': 0.267766, 'conversion_share': 0.020376}, {'asin': 'B007BVMVDU', 'click_share': 0.177746, 'conversion_share': 0.18181799999999998}, {'asin': 'B007HJ84ZK', 'click_share': 0.08209, 'conversion_share': 0.00627}]}, {'x': '2019-04-22', 'y': 42, 'products': [{'asin': 'B07KGL9531', 'click_share': 0.267766, 'conversion_share': 0.020376}, {'asin': 'B007BVMVDU', 'click_share': 0.177746, 'conversion_share': 0.18181799999999998}, {'asin': 'B007HJ84ZK', 'click_share': 0.08209, 'conversion_share': 0.00627}]}, {'x': '2019-04-23', 'y': 85, 'products': [{'asin': 'B07KGL9531', 'click_share': 0.267766, 'conversion_share': 0.020376}, {'asin': 'B007BVMVDU', 'click_share': 0.177746, 'conversion_share': 0.18181799999999998}, {'asin': 'B007HJ84ZK', 'click_share': 0.08209, 'conversion_share': 0.00627}]}, {'x': '2019-04-24', 'y': 16, 'products': [{'asin': 'B07KGL9531', 'click_share': 0.267766, 'conversion_share': 0.020376}, {'asin': 'B007BVMVDU', 'click_share': 0.177746, 'conversion_share': 0.18181799999999998}, {'asin': 'B007HJ84ZK', 'click_share': 0.08209, 'conversion_share': 0.00627}]}, {'x': '2019-04-25', 'y': 54, 'products': [{'asin': 'B07KGL9531', 'click_share': 0.267766, 'conversion_share': 0.020376}, {'asin': 'B007BVMVDU', 'click_share': 0.177746, 'conversion_share': 0.18181799999999998}, {'asin': 'B007HJ84ZK', 'click_share': 0.08209, 'conversion_share': 0.00627}]}, {'x': '2019-04-26', 'y': 62, 'products': [{'asin': 'B07KGL9531', 'click_share': 0.267766, 'conversion_share': 0.020376}, {'asin': 'B007BVMVDU', 'click_share': 0.177746, 'conversion_share': 0.18181799999999998}, {'asin': 'B007HJ84ZK', 'click_share': 0.08209, 'conversion_share': 0.00627}]}, {'x': '2019-04-27', 'y': 93, 'products': [{'asin': 'B07KGL9531', 'click_share': 0.267766, 'conversion_share': 0.020376}, {'asin': 'B007BVMVDU', 'click_share': 0.177746, 'conversion_share': 0.18181799999999998}, {'asin': 'B007HJ84ZK', 'click_share': 0.08209, 'conversion_share': 0.00627}]}, {'x': '2019-04-28', 'y': 74, 'products': [{'asin': 'B07KGL9531', 'click_share': 0.267766, 'conversion_share': 0.020376}, {'asin': 'B007BVMVDU', 'click_share': 0.177746, 'conversion_share': 0.18181799999999998}, {'asin': 'B007HJ84ZK', 'click_share': 0.08209, 'conversion_share': 0.00627}]}, {'x': '2019-04-29', 'y': 41, 'products': [{'asin': 'B07KGL9531', 'click_share': 0.267766, 'conversion_share': 0.020376}, {'asin': 'B007BVMVDU', 'click_share': 0.177746, 'conversion_share': 0.18181799999999998}, {'asin': 'B007HJ84ZK', 'click_share': 0.08209, 'conversion_share': 0.00627}]}, {'x': '2019-04-30', 'y': 49, 'products': [{'asin': 'B07KGL9531', 'click_share': 0.267766, 'conversion_share': 0.020376}, {'asin': 'B007BVMVDU', 'click_share': 0.177746, 'conversion_share': 0.18181799999999998}, {'asin': 'B007HJ84ZK', 'click_share': 0.08209, 'conversion_share': 0.00627}]}]}, {'id': 'headphones', 'color': 'hsl(69, 70%, 80%)', 'data': [{'x': '2019-04-01', 'y': 34, 'products': [{'asin': 'B07MXBF9ZS', 'click_share': 0.069159, 'conversion_share': 0.006999}, {'asin': 'B07R2B96RC', 'click_share': 0.06439, 'conversion_share': 0.0}, {'asin': 'B07PJWF7KD', 'click_share': 0.061877, 'conversion_share': 0.007873999999999999}]}, {'x': '2019-04-02', 'y': 18, 'products': [{'asin': 'B07MXBF9ZS', 'click_share': 0.069159, 'conversion_share': 0.006999}, {'asin': 'B07R2B96RC', 'click_share': 0.06439, 'conversion_share': 0.0}, {'asin': 'B07PJWF7KD', 'click_share': 0.061877, 'conversion_share': 0.007873999999999999}]}, {'x': '2019-04-03', 'y': 83, 'products': [{'asin': 'B07MXBF9ZS', 'click_share': 0.069159, 'conversion_share': 0.006999}, {'asin': 'B07R2B96RC', 'click_share': 0.06439, 'conversion_share': 0.0}, {'asin': 'B07PJWF7KD', 'click_share': 0.061877, 'conversion_share': 0.007873999999999999}]}, {'x': '2019-04-04', 'y': 82, 'products': [{'asin': 'B07MXBF9ZS', 'click_share': 0.069159, 'conversion_share': 0.006999}, {'asin': 'B07R2B96RC', 'click_share': 0.06439, 'conversion_share': 0.0}, {'asin': 'B07PJWF7KD', 'click_share': 0.061877, 'conversion_share': 0.007873999999999999}]}, {'x': '2019-04-05', 'y': 71, 'products': [{'asin': 'B07MXBF9ZS', 'click_share': 0.069159, 'conversion_share': 0.006999}, {'asin': 'B07R2B96RC', 'click_share': 0.06439, 'conversion_share': 0.0}, {'asin': 'B07PJWF7KD', 'click_share': 0.061877, 'conversion_share': 0.007873999999999999}]}, {'x': '2019-04-06', 'y': 12, 'products': [{'asin': 'B07MXBF9ZS', 'click_share': 0.069159, 'conversion_share': 0.006999}, {'asin': 'B07R2B96RC', 'click_share': 0.06439, 'conversion_share': 0.0}, {'asin': 'B07PJWF7KD', 'click_share': 0.061877, 'conversion_share': 0.007873999999999999}]}, {'x': '2019-04-07', 'y': 11, 'products': [{'asin': 'B07MXBF9ZS', 'click_share': 0.069159, 'conversion_share': 0.006999}, {'asin': 'B07R2B96RC', 'click_share': 0.06439, 'conversion_share': 0.0}, {'asin': 'B07PJWF7KD', 'click_share': 0.061877, 'conversion_share': 0.007873999999999999}]}, {'x': '2019-04-08', 'y': 48, 'products': [{'asin': 'B07MXBF9ZS', 'click_share': 0.069159, 'conversion_share': 0.006999}, {'asin': 'B07R2B96RC', 'click_share': 0.06439, 'conversion_share': 0.0}, {'asin': 'B07PJWF7KD', 'click_share': 0.061877, 'conversion_share': 0.007873999999999999}]}, {'x': '2019-04-09', 'y': 61, 'products': [{'asin': 'B07MXBF9ZS', 'click_share': 0.069159, 'conversion_share': 0.006999}, {'asin': 'B07R2B96RC', 'click_share': 0.06439, 'conversion_share': 0.0}, {'asin': 'B07PJWF7KD', 'click_share': 0.061877, 'conversion_share': 0.007873999999999999}]}, {'x': '2019-04-10', 'y': 14, 'products': [{'asin': 'B07MXBF9ZS', 'click_share': 0.069159, 'conversion_share': 0.006999}, {'asin': 'B07R2B96RC', 'click_share': 0.06439, 'conversion_share': 0.0}, {'asin': 'B07PJWF7KD', 'click_share': 0.061877, 'conversion_share': 0.007873999999999999}]}, {'x': '2019-04-11', 'y': 94, 'products': [{'asin': 'B07MXBF9ZS', 'click_share': 0.069159, 'conversion_share': 0.006999}, {'asin': 'B07R2B96RC', 'click_share': 0.06439, 'conversion_share': 0.0}, {'asin': 'B07PJWF7KD', 'click_share': 0.061877, 'conversion_share': 0.007873999999999999}]}, {'x': '2019-04-12', 'y': 49, 'products': [{'asin': 'B07MXBF9ZS', 'click_share': 0.069159, 'conversion_share': 0.006999}, {'asin': 'B07R2B96RC', 'click_share': 0.06439, 'conversion_share': 0.0}, {'asin': 'B07PJWF7KD', 'click_share': 0.061877, 'conversion_share': 0.007873999999999999}]}, {'x': '2019-04-13', 'y': 95, 'products': [{'asin': 'B07MXBF9ZS', 'click_share': 0.069159, 'conversion_share': 0.006999}, {'asin': 'B07R2B96RC', 'click_share': 0.06439, 'conversion_share': 0.0}, {'asin': 'B07PJWF7KD', 'click_share': 0.061877, 'conversion_share': 0.007873999999999999}]}, {'x': '2019-04-14', 'y': 40, 'products': [{'asin': 'B07MXBF9ZS', 'click_share': 0.069159, 'conversion_share': 0.006999}, {'asin': 'B07R2B96RC', 'click_share': 0.06439, 'conversion_share': 0.0}, {'asin': 'B07PJWF7KD', 'click_share': 0.061877, 'conversion_share': 0.007873999999999999}]}, {'x': '2019-04-15', 'y': 2, 'products': [{'asin': 'B07MXBF9ZS', 'click_share': 0.069159, 'conversion_share': 0.006999}, {'asin': 'B07R2B96RC', 'click_share': 0.06439, 'conversion_share': 0.0}, {'asin': 'B07PJWF7KD', 'click_share': 0.061877, 'conversion_share': 0.007873999999999999}]}, {'x': '2019-04-16', 'y': 86, 'products': [{'asin': 'B07MXBF9ZS', 'click_share': 0.069159, 'conversion_share': 0.006999}, {'asin': 'B07R2B96RC', 'click_share': 0.06439, 'conversion_share': 0.0}, {'asin': 'B07PJWF7KD', 'click_share': 0.061877, 'conversion_share': 0.007873999999999999}]}, {'x': '2019-04-17', 'y': 68, 'products': [{'asin': 'B07MXBF9ZS', 'click_share': 0.069159, 'conversion_share': 0.006999}, {'asin': 'B07R2B96RC', 'click_share': 0.06439, 'conversion_share': 0.0}, {'asin': 'B07PJWF7KD', 'click_share': 0.061877, 'conversion_share': 0.007873999999999999}]}, {'x': '2019-04-18', 'y': 73, 'products': [{'asin': 'B07MXBF9ZS', 'click_share': 0.069159, 'conversion_share': 0.006999}, {'asin': 'B07R2B96RC', 'click_share': 0.06439, 'conversion_share': 0.0}, {'asin': 'B07PJWF7KD', 'click_share': 0.061877, 'conversion_share': 0.007873999999999999}]}, {'x': '2019-04-19', 'y': 83, 'products': [{'asin': 'B07MXBF9ZS', 'click_share': 0.069159, 'conversion_share': 0.006999}, {'asin': 'B07R2B96RC', 'click_share': 0.06439, 'conversion_share': 0.0}, {'asin': 'B07PJWF7KD', 'click_share': 0.061877, 'conversion_share': 0.007873999999999999}]}, {'x': '2019-04-20', 'y': 29, 'products': [{'asin': 'B07MXBF9ZS', 'click_share': 0.069159, 'conversion_share': 0.006999}, {'asin': 'B07R2B96RC', 'click_share': 0.06439, 'conversion_share': 0.0}, {'asin': 'B07PJWF7KD', 'click_share': 0.061877, 'conversion_share': 0.007873999999999999}]}, {'x': '2019-04-21', 'y': 77, 'products': [{'asin': 'B07MXBF9ZS', 'click_share': 0.069159, 'conversion_share': 0.006999}, {'asin': 'B07R2B96RC', 'click_share': 0.06439, 'conversion_share': 0.0}, {'asin': 'B07PJWF7KD', 'click_share': 0.061877, 'conversion_share': 0.007873999999999999}]}, {'x': '2019-04-22', 'y': 43, 'products': [{'asin': 'B07MXBF9ZS', 'click_share': 0.069159, 'conversion_share': 0.006999}, {'asin': 'B07R2B96RC', 'click_share': 0.06439, 'conversion_share': 0.0}, {'asin': 'B07PJWF7KD', 'click_share': 0.061877, 'conversion_share': 0.007873999999999999}]}, {'x': '2019-04-23', 'y': 43, 'products': [{'asin': 'B07MXBF9ZS', 'click_share': 0.069159, 'conversion_share': 0.006999}, {'asin': 'B07R2B96RC', 'click_share': 0.06439, 'conversion_share': 0.0}, {'asin': 'B07PJWF7KD', 'click_share': 0.061877, 'conversion_share': 0.007873999999999999}]}, {'x': '2019-04-24', 'y': 15, 'products': [{'asin': 'B07MXBF9ZS', 'click_share': 0.069159, 'conversion_share': 0.006999}, {'asin': 'B07R2B96RC', 'click_share': 0.06439, 'conversion_share': 0.0}, {'asin': 'B07PJWF7KD', 'click_share': 0.061877, 'conversion_share': 0.007873999999999999}]}, {'x': '2019-04-25', 'y': 54, 'products': [{'asin': 'B07MXBF9ZS', 'click_share': 0.069159, 'conversion_share': 0.006999}, {'asin': 'B07R2B96RC', 'click_share': 0.06439, 'conversion_share': 0.0}, {'asin': 'B07PJWF7KD', 'click_share': 0.061877, 'conversion_share': 0.007873999999999999}]}, {'x': '2019-04-26', 'y': 45, 'products': [{'asin': 'B07MXBF9ZS', 'click_share': 0.069159, 'conversion_share': 0.006999}, {'asin': 'B07R2B96RC', 'click_share': 0.06439, 'conversion_share': 0.0}, {'asin': 'B07PJWF7KD', 'click_share': 0.061877, 'conversion_share': 0.007873999999999999}]}, {'x': '2019-04-27', 'y': 79, 'products': [{'asin': 'B07MXBF9ZS', 'click_share': 0.069159, 'conversion_share': 0.006999}, {'asin': 'B07R2B96RC', 'click_share': 0.06439, 'conversion_share': 0.0}, {'asin': 'B07PJWF7KD', 'click_share': 0.061877, 'conversion_share': 0.007873999999999999}]}, {'x': '2019-04-28', 'y': 61, 'products': [{'asin': 'B07MXBF9ZS', 'click_share': 0.069159, 'conversion_share': 0.006999}, {'asin': 'B07R2B96RC', 'click_share': 0.06439, 'conversion_share': 0.0}, {'asin': 'B07PJWF7KD', 'click_share': 0.061877, 'conversion_share': 0.007873999999999999}]}, {'x': '2019-04-29', 'y': 82, 'products': [{'asin': 'B07MXBF9ZS', 'click_share': 0.069159, 'conversion_share': 0.006999}, {'asin': 'B07R2B96RC', 'click_share': 0.06439, 'conversion_share': 0.0}, {'asin': 'B07PJWF7KD', 'click_share': 0.061877, 'conversion_share': 0.007873999999999999}]}, {'x': '2019-04-30', 'y': 94, 'products': [{'asin': 'B07MXBF9ZS', 'click_share': 0.069159, 'conversion_share': 0.006999}, {'asin': 'B07R2B96RC', 'click_share': 0.06439, 'conversion_share': 0.0}, {'asin': 'B07PJWF7KD', 'click_share': 0.061877, 'conversion_share': 0.007873999999999999}]}]}]

};

function getFormattedDate(date) {
    var year = date.getFullYear();
    var month = (1 + date.getMonth()).toString();
    month = month.length > 1 ? month : '0' + month;
    var day = date.getDate().toString();
    day = day.length > 1 ? day : '0' + day;
    return year + '-' + month + '-' + day;
}

function getDaysFromDates(startDate, endDate) {
    let startDt = new Date(startDate);
    let endDt = new Date(endDate);
    let daysOfYear = [];
    for (var d = startDt; d <= endDt; d.setDate(d.getDate() + 1)) {
        daysOfYear.push(getFormattedDate(new Date(d)));
    }
    return daysOfYear;
}

function getData(term, startDate, endDate) {
    let days = getDaysFromDates(startDate, endDate);
    let chartData = {};
    if(term == null)
        term = "";
    if (TERMS.indexOf(term.toLowerCase()) !== -1){
        chartData['id'] = term;
    }else{
        chartData['id'] = TERMS[getRandomArbitrary(0, TERMS.length)];
    }
    chartData['color'] = COLORS[getRandomArbitrary(0, COLORS.length)];
    chartData['data'] = [];
    for(var i = 0; i<days.length; i++){
        let dayObject = {x : days[i], y: getRandomArbitrary(1,10), products : []};
        for(var j = 0; j<3; j++){
            dayObject.products.push(
                {
                    asin : ASINS[getRandomArbitrary(0, ASINS.length)],
                    click_share : getRandomProcent(),
                    conversion_share : getRandomProcent()
                }
            )
        }
        chartData.data.push(dayObject);
    }
    return chartData;
}

export default getData;