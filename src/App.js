import React, {Component} from 'react';
import './App.css';
import LineChart from "./Components/LineChart";
import Switches from "./Components/Switches";
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import WebsiteLogo from './logo/logo_transparent.png'
import DatePicker from "./Components/DatePicker";
import SimpleCard from "./Components/SimpleCard";
import getData from "./Data";

class App extends Component {
    constructor(props) {
        super(props);
        let endDT = this.getFormattedDate(new Date());
        let yesterday = new Date();
        yesterday.setDate(yesterday.getDate() - 7);
        let startDT = this.getFormattedDate(yesterday);
        this.state = {
            data : [getData("null", startDT, endDT)],
            searchText : "",
            startDate : startDT,
            endDate : endDT,
            enableArea : true,
            showTop3Results : false,
            products : []
        };

        this.searchButtonOnClick = this.searchButtonOnClick.bind(this);
        this.getFormattedDate = this.getFormattedDate.bind(this);
        this.searchBoxChange = this.searchBoxChange.bind(this);
        this.startDateGetter = this.startDateGetter.bind(this);
        this.endDateGetter = this.endDateGetter.bind(this);
        this.top5OnClick = this.top5OnClick.bind(this);
        this.setAreaState = this.setAreaState.bind(this);
        this.setPosition = this.setPosition.bind(this);
        this.rewriteGraphWithProduct = this.rewriteGraphWithProduct.bind(this);
    }

    getFormattedDate(date) {
        var year = date.getFullYear();
        var month = (1 + date.getMonth()).toString();
        month = month.length > 1 ? month : '0' + month;
        var day = date.getDate().toString();
        day = day.length > 1 ? day : '0' + day;
        return year + '-' + month + '-' + day;
    }

    searchButtonOnClick() {
        let getdata = getData(this.state.searchText, this.state.startDate, this.state.endDate);
        this.setState({data : [getdata]});
    }

    top5OnClick() {
        let getdata1 = getData("", this.state.startDate, this.state.endDate);
        let getdata2 = getData("", this.state.startDate, this.state.endDate);
        let getdata3 = getData("", this.state.startDate, this.state.endDate);
        this.setState({data : [getdata1, getdata2, getdata3]});
    }

    searchBoxChange(e) {
        this.setState({searchText : e.target.value});
    }

    startDateGetter(date) {
        this.setState({startDate : date});
    }

    endDateGetter(date) {
        this.setState({endDate : date});
    }

    setAreaState(data) {
        this.setState({enableArea : data})
    }

    setPosition(position) {
        let obj = [];
        for(var i=0; i<this.state.data.length; i++){
            let elem = this.state.data[i];
            if(elem.id === position.serieId){
                for(var j=0; j<elem.data[position.index].products.length; j++){
                    var product = elem.data[position.index].products[j];
                    product['link'] = "https://www.amazon.com/dp/"+product.asin;
                    product['date'] = elem.data[position.index].x;
                    obj.push(product);
                }
            }
        }
        this.setState({showTop3Results : true, products : obj})
    }

    rewriteGraphWithProduct(data) {
        alert("Now i should rewrite the graph for product with asin " + data)
    }

    render() {
        return (
            <div>
                <div className="header">

                    <div className="row">
                        <div className="headerColumn">
                            <img src={WebsiteLogo} className="logo" alt="Logo"/>
                        </div>
                        <div className="headerColumn"
                             style={{display: "flex", marginTop: "35px", paddingLeft: "150px"}}>
                            <DatePicker writeDate={this.startDateGetter.bind(this)} id={"startDateTimePicker"} label={"Start Date"}></DatePicker>

                            <DatePicker writeDate={this.endDateGetter.bind(this)} id={"endDateTimePicker"} label={"End Date"}></DatePicker>
                        </div>
                        <div className="headerColumn" style={{marginTop: "30px"}}>
                            <TextField
                                id="standard-dense"
                                label="AMZ Search Term"
                                className="textField dense"
                                margin="dense"
                                onChange={this.searchBoxChange}
                            />
                            <Button variant="contained" color="primary" style={{margin: "10px", marginTop: "15px"}}
                                    className="button" onClick={this.searchButtonOnClick}>
                                Search
                            </Button>

                            <Button variant="contained" id="top5" color="primary"
                                    style={{margin: "10px", marginTop: "15px"}} className="button" onClick={this.top5OnClick}>
                                Top 3 Keywords
                            </Button>
                        </div>
                    </div>
                    <div className="row">
                        <div className="headerColumn">
                            <p></p>
                        </div>
                        <div className="headerColumn">
                            <p></p>
                        </div>
                        <div className="headerColumn">
                            <div className="row" style={{display : "flex", border : "0px", padding : "0px"}}>
                                <p>
                                    Disable area:
                                </p>
                                <Switches writeAreaToggleValue={this.setAreaState.bind(this)}></Switches>
                            </div>

                        </div>
                    </div>
                </div>
                { this.state.showTop3Results ? <Products rewriteGraphWithProduct={this.rewriteGraphWithProduct.bind(this)} products={this.state.products}/> : null }
                <div className="App">

                    <div className="row">
                        <LineChart setPosition={this.setPosition.bind(this)} enableArea={this.state.enableArea} data={this.state.data}></LineChart>
                        <div className="column">
                        </div>
                        <div className="column"></div>
                    </div>

                </div>
            </div>
        );
    }
}

class Products extends Component{
    constructor(props){
        super(props);
        this.rewriteGraphWithProduct = this.rewriteGraphWithProduct.bind(this);
    }

    rewriteGraphWithProduct(data) {
        this.props.rewriteGraphWithProduct(data);
    }

    render() {
        return (

            <div id="top4ProductsDiv" className="row">
                <div className="headerBallonColumn">
                    <SimpleCard rewriteGraphWithProduct={this.rewriteGraphWithProduct.bind(this)} product={this.props.products[0]}></SimpleCard>
                </div>
                <div className="headerBallonColumn">
                    <SimpleCard rewriteGraphWithProduct={this.rewriteGraphWithProduct.bind(this)} product={this.props.products[1]}></SimpleCard>
                </div>
                <div className="headerBallonColumn">
                    <SimpleCard rewriteGraphWithProduct={this.rewriteGraphWithProduct.bind(this)} product={this.props.products[2]}></SimpleCard>
                </div>
            </div>
        );
    }
}
export default App;
